import Typography from "typography"
import noriega from "typography-theme-noriega"

//Font
const typography = new Typography(noriega)

// Export helper functions
export const { scale, rhythm, options } = typography
export default typography
