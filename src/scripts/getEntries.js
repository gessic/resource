// Functions of getting different content from contentful

const contentful = require('contentful');

//Fetching content according to environment
const client = contentful.createClient({
    space: process.env.GATSBY_spaceId,
    accessToken: process.env.GATSBY_accessToken ,
    host: process.env.GATSBY_host
})

//Gets specific Profile from contentful
export const getProfileEntry = async (slug) => {
    try {
        const entry = await client.getEntries({
            content_type: 'profile',
            'fields.slug': slug,
        });
        return entry.items[0].fields;
    } catch (error) {
        console.log(error)
    }
}

//Gets page contents from contentful
export const getPage = async (slug) => {
    try {
        const entry = await client.getEntries({
            content_type: 'page',
            'fields.slug': slug,
            include: 3
        });
        return entry.items[0].fields;
    } catch (error) {
        console.log(error)
    }
}

//Gets specific entry from contentful
export const getEntry = async (id) => {
    try {
        return await client.getEntry(id);
    } catch (error) {
        console.log(error)
    }
}

//Gets all profiles from contentful
export const getProfiles = async () => {
    try {
        return (await client.getEntries({
            content_type: 'profile'
        })).items
    }catch (error) {
        console.log("Error getting profiles: ", error)
    }
}



