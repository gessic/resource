import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import RichTextRenderer from "../../renderer/RichTextRenderer";

const CardWrapper = styled.div`
  margin-top: 2em;
`;

const TextBox = styled.div`
  padding: 50px 50px 100px 50px;
  border-radius: 4px;
  font-size: 20px;
  box-shadow: 5px 5px 15px #ddd, -5px -5px 15px #ddd;
  background: white;
  max-width: 900px;
  margin: auto;
  display: flex;
  flex-direction: column;
  @media (max-width: 768px) {
    font-size: 12px;
  }
  p {
    margin-top: 20px;
  }
  li {
    margin-top: 5px;
  }
  ul {
    margin-top: 40px;
  }
  h1 {
    font-size: 40px;
    font-weight: bold;
  }
`;

const Card = ({content, ctaLink}) => {
    const {name, alignment, navigation} = ctaLink ? ctaLink?.fields : null;
    const align = alignment === 'Left' ? 'is-align-self-flex-start' : alignment === 'Right' ? 'is-align-self-flex-end' : 'is-align-self-center';
    return (
        <CardWrapper>
            <TextBox>
                {RichTextRenderer.render(content)}
                {ctaLink && !navigation &&
                    <button onClick={() => window.location = "https://eso.abaca.app/entrepreneurs/program/?a=resource2"}
                            style={{
                                backgroundColor: '#4c9caf',
                                fontWeight: 'bold',
                                marginTop: 40,
                                padding: '25px 40px'
                            }}
                            className={`button is-info is-flex ${align}`}>
                        {name}
                    </button>}
            </TextBox>
        </CardWrapper>
    );
};


Card.propTypes = {
    content: PropTypes.object.isRequired,
    ctaLink: PropTypes.object,
};

export default Card;
