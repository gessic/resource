import React from "react";
import styled from "styled-components";

const ProfileItemWrapper = styled.div`
  //box-shadow: 5px 5px 15px #ddd, -5px -5px 15px #ddd;
  max-width: 400px;
  margin: ${props => props.lastItem ? '2em 2em' : '2em auto'};
  border-radius: 0.5em;
  display: flex;
  flex-direction: column;
  @media (min-width: 1024px) and (max-width: 1280px) {
    max-width: 30%;
    margin: 2em .5em;
  }
  @media (min-width: 1281px){
    max-width: 29%;
  }
  @media (max-width: 768px) {
    max-width: 400px;
  }
  @media (max-width: 576px) {
    max-width: 280px;
    margin: 2em 2em;
  }
  @media (max-width: 281px) {
    max-width: 200px;
  }
  
`
const Image = styled.img`
  max-width: 100%;
  display: flex;
  height: auto;
  min-height: 240px;
  //padding: 1em 1em 0;
  //border-radius: 1.5em;
  @media (max-width: 768px) {
    padding-bottom: 0;
    min-height: unset;
  }
  @media(min-width: 768px) and (max-width: 998px){
    min-height: unset;
  }
  @media (max-width: 576px) {
    width: 100%;
    height: auto;
    min-height: unset;
  }

`
const TextArea = styled.div`
  display: flex;
  flex-direction: column;
`
const Body = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
  padding-top: 1em;
`

const Header = styled.div`
  display: flex;
  justify-content: space-between;
  padding-bottom: 1em;

`
const NameOrg = styled.div`
  padding-top: 1em;
  //padding-left: 1em;
  @media(max-width: 1280px) and (min-width: 1024px){
    h1{
      /***/
      font-size: 1.15rem !important;
    }
  }
  @media(max-width: 1311px) and (min-width: 1281px){
    h1{
      font-size: ${props => props?.fontSize}rem !important;
    }
  }
  h1{
    font-size: 1.4rem;
  }
  h1, h2 {
    color: #62A3B0;
  }
  @media (max-width: 768px) {
    padding-top: 0;
    h2 {
      padding-top: 0.5em;
    }
  }
`
const Logo = styled.img`
  max-height: 20%;
  max-width: 200px;
  position: absolute;
  bottom: 0;
  right: 0;
  background-color: #FFFFFF;
  padding: .5em;
  @media(min-width: 998px){
    max-height: 12%;
  }
  @media (max-width: 576px){
    max-height: 60px;
  }
`
const IntroText = styled.div`
  p{
    //font-size: 1.2em;
  }
  @media (max-width: 768px) {
   
    padding-top: 0.5em;
  }
`

const CTALink = styled.div`
  padding-top: 2em;
  padding-bottom: 2em;

  a {
    border: 1px solid #3593A5;
    padding: 0.5em;
    border-radius: 0.25em;
    color: #3593A5;
  }

  a:hover {
    color: white;
    background-color: #3593A5;
  }
`
const ImageContainer = styled.div`
  position: relative;
`
export const Line = styled.div`

  border-top: 1px solid #62A3B0;
`

const ProfileItem = (props) => {
    const {profilePhoto, organizationName, profileName, profileCompanyLogo, keyStats, slug, profileNameFont, lastItem} = props;
    return (
        <ProfileItemWrapper lastItem={lastItem}>
            <ImageContainer >
                <Image
                    className='imageContainer'
                    src={`https:${profilePhoto?.fields?.file?.url}`}
                    alt={profilePhoto?.fields?.title}
                />
                <Logo
                    src={`https:${profileCompanyLogo?.fields?.file?.url}`}
                    alt={profileCompanyLogo.fields.title}
                />
            </ImageContainer>
            <TextArea>
                <Header>
                    <NameOrg fontSize={profileNameFont}>
                        <h1 className="title">{profileName}</h1>
                        <h2 className="subtitle">{organizationName}</h2>
                    </NameOrg>
                </Header>
                <Line/>
                <Body>
                    <IntroText>
                        <p>
                            {keyStats[2]?.value ? keyStats[2]?.value : ""}
                        </p>
                    </IntroText>
                    <CTALink>
                        <a href={`${slug}`}>Read Story</a>
                    </CTALink>
                </Body>
            </TextArea>
        </ProfileItemWrapper>
    )
}

ProfileItem.propTypes = {};

export default ProfileItem;
