import React from "react"
import styled from "styled-components"


import CohortItem from './CohortItem'
import CohortList from './CohortList.json'



export default function Cohorts() {
    return (
        <CohortsSection className={`section is-flex is-flex-direction-column`}>

            <div className='container'>
                <h1 className="title" > Meet the Ecosystem Leaders </h1>
                <div className="columns is-mobile is-multiline is-centered">
                    {CohortList.map(cohort => {
                        if(cohort?.hasMultipleCohorts){
                            return cohort.team.map(teamMember => {
                                return <CohortItem cohort={teamMember} />
                            })
                        }
                        else
                            return <CohortItem cohort={cohort} />
                    })}
                </div>
            </div>
        </CohortsSection>
    )
}
 const CohortsSection = styled.section` 
    background-color: #e1e4d5;
    h1{
        padding-bottom: 5px;
        text-align: center
    }
 `
