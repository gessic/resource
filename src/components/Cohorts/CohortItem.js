import React, {useState} from "react"
import './image.css'


export default function CohortItem(props) {
    const [isActive, setIsActive] = useState(false)

    return (
        <div className="column cohort-item is-3-desktop is-6-mobile">
            <div style={{position: 'relative', minWidth: 175}}>
                <img className={`${isActive ? "hover" : "nonHover"} cohort-img`}
                     onMouseEnter={() => setIsActive(true)}
                     onMouseLeave={() => setIsActive(false)}
                     src={props?.cohort?.image}
                     alt={props?.cohort?.name}
                     role='button'
                />
            </div>
            <h1 style={{fontWeight: 'bold', textAlign: 'center'}}>  {props?.cohort?.name} </h1>
            {props?.cohort?.hasProfile &&
                <a style={{textDecoration: 'none', color: 'inherit'}}
                   href={props?.cohort?.internalURL}>
                    <h1 style={{fontWeight: 'bold', textAlign: 'center',}}>
                        <u> View Profile </u>
                    </h1>
                </a>}
            <h3 style={{textAlign: 'center', paddingTop: 5}}>
                <a style={{textDecoration: 'none', color: 'inherit'}}
                   href={props?.cohort?.url} target="_blank"
                   rel="noreferrer"> {props?.cohort?.company} </a>
            </h3>
            <h2 style={{textAlign: 'center', paddingTop: 5}}> {props?.cohort?.position} </h2>
        </div>
    )
}
