import React, {useEffect, useState} from "react"
import styled from "styled-components"
import "@fontsource/merriweather";
import "@fontsource/merriweather-sans";
import {getPage, getProfileEntry} from '../../scripts/getEntries';
import {BLOCKS, MARKS} from "@contentful/rich-text-types";
import {documentToReactComponents} from "@contentful/rich-text-react-renderer";
import ImageParagraph from "../ImageParagraph";


const LinkedInLogo = (props) => (
    <svg fill={props.fill} className={props.class} xmlns="http://www.w3.org/2000/svg" width="25" height="25"
         viewBox="0 0 38 38">
        <g id="Icon_" data-name="Icon " transform="translate(0.094 0.094)">
            <rect id="Area_ICON:feather_linkedin_SIZE:MEDIUM_STYLE:STYLE2_"
                  data-name="Area [ICON:feather/linkedin][SIZE:MEDIUM][STYLE:STYLE2]" width="38" height="38"
                  transform="translate(-0.094 -0.094)" fill="rgba(253,73,198,0.35)" opacity="0"/>
            <g id="Icon" transform="translate(3.159 3.159)">
                <path id="Path"
                      d="M17.81,6.667a9.475,9.475,0,0,1,9.477,9.477V27.2H20.969V16.144a3.159,3.159,0,1,0-6.317,0V27.2H8.333V16.144A9.476,9.476,0,0,1,17.81,6.667Z"
                      transform="translate(4.302 2.81)"/>
                <rect id="Rect" width="6.318" height="18.953" transform="translate(0 11.056)"/>
                <circle id="Path-2" data-name="Path" cx="3.159" cy="3.159" r="3.159" transform="translate(0 0)"
                />
            </g>
        </g>
    </svg>
)

const TwitterLogo = (props) => (
    <svg fill={props.fill} className={props.class} xmlns="http://www.w3.org/2000/svg" width="25" height="25"
         viewBox="0 0 38.209 37">
        <g id="Icon_" data-name="Icon " transform="translate(-0.057 -0.122)">
            <rect id="Area_ICON:feather_twitter_SIZE:MEDIUM_STYLE:STYLE2_"
                  data-name="Area [ICON:feather/twitter][SIZE:MEDIUM][STYLE:STYLE2]" width="38" height="37"
                  transform="translate(0.266 0.122)" fill="rgba(253,73,198,0.35)" opacity="0"/>
            <g id="Icon" transform="translate(0.891 2.7)">
                <path id="Path"
                      d="M37.3,2.51a17.662,17.662,0,0,1-5.206,2.7,7.133,7.133,0,0,0-8.251-2.2A7.91,7.91,0,0,0,19.067,10.5V12.26A17.353,17.353,0,0,1,4.149,4.272s-6.63,15.87,8.289,22.925a18.455,18.455,0,0,1-11.6,3.525c14.918,8.817,33.153,0,33.153-20.277a8.4,8.4,0,0,0-.133-1.464A13.839,13.839,0,0,0,37.3,2.51Z"
                      transform="translate(-0.833 -2.491)"/>
            </g>
        </g>
    </svg>

)

const WebLogo = () => (
    <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="currentColor" className="bi bi-globe2"
         viewBox="0 0 16 16">
        <path
            d="M0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8zm7.5-6.923c-.67.204-1.335.82-1.887 1.855-.143.268-.276.56-.395.872.705.157 1.472.257 2.282.287V1.077zM4.249 3.539c.142-.384.304-.744.481-1.078a6.7 6.7 0 0 1 .597-.933A7.01 7.01 0 0 0 3.051 3.05c.362.184.763.349 1.198.49zM3.509 7.5c.036-1.07.188-2.087.436-3.008a9.124 9.124 0 0 1-1.565-.667A6.964 6.964 0 0 0 1.018 7.5h2.49zm1.4-2.741a12.344 12.344 0 0 0-.4 2.741H7.5V5.091c-.91-.03-1.783-.145-2.591-.332zM8.5 5.09V7.5h2.99a12.342 12.342 0 0 0-.399-2.741c-.808.187-1.681.301-2.591.332zM4.51 8.5c.035.987.176 1.914.399 2.741A13.612 13.612 0 0 1 7.5 10.91V8.5H4.51zm3.99 0v2.409c.91.03 1.783.145 2.591.332.223-.827.364-1.754.4-2.741H8.5zm-3.282 3.696c.12.312.252.604.395.872.552 1.035 1.218 1.65 1.887 1.855V11.91c-.81.03-1.577.13-2.282.287zm.11 2.276a6.696 6.696 0 0 1-.598-.933 8.853 8.853 0 0 1-.481-1.079 8.38 8.38 0 0 0-1.198.49 7.01 7.01 0 0 0 2.276 1.522zm-1.383-2.964A13.36 13.36 0 0 1 3.508 8.5h-2.49a6.963 6.963 0 0 0 1.362 3.675c.47-.258.995-.482 1.565-.667zm6.728 2.964a7.009 7.009 0 0 0 2.275-1.521 8.376 8.376 0 0 0-1.197-.49 8.853 8.853 0 0 1-.481 1.078 6.688 6.688 0 0 1-.597.933zM8.5 11.909v3.014c.67-.204 1.335-.82 1.887-1.855.143-.268.276-.56.395-.872A12.63 12.63 0 0 0 8.5 11.91zm3.555-.401c.57.185 1.095.409 1.565.667A6.963 6.963 0 0 0 14.982 8.5h-2.49a13.36 13.36 0 0 1-.437 3.008zM14.982 7.5a6.963 6.963 0 0 0-1.362-3.675c-.47.258-.995.482-1.565.667.248.92.4 1.938.437 3.008h2.49zM11.27 2.461c.177.334.339.694.482 1.078a8.368 8.368 0 0 0 1.196-.49 7.01 7.01 0 0 0-2.275-1.52c.218.283.418.597.597.932zm-.488 1.343a7.765 7.765 0 0 0-.395-.872C9.835 1.897 9.17 1.282 8.5 1.077V4.09c.81-.03 1.577-.13 2.282-.287z"/>
    </svg>
)
const Logos = styled.div`
  padding-top: 1em;
  display: flex;
  justify-content: center;
  margin: 1em 4em;
`;

//Profile Component used in individual company pages
export default function Profile({slug}) {
    const [entry, setEntry] = useState({})
    const [loading, setLoading] = useState(true);
    const [twitterIconColor, setTwitterIconColor] = useState('#e3f1f3');
    const [linkedinIconColor, setLinkedinIconColor] = useState('#e3f1f3');
    const [webIconColor, setWebIconColor] = useState('#e3f1f3');
    const [profiles, setProfiles] = useState([])
    const dark = '#009db0'
    const norm = '#e3f1f3'

    useEffect(() => {
        getProfileEntry(slug)
            .then(entry => {
                setEntry(entry)
                typeof entry === 'undefined' ? setLoading(true) : setLoading(false);
            })
    }, [slug])

    useEffect(() => {
        if (loading) {
            getPage('/profiles')
                .then(prof => {
                    const {pageContent} = prof;
                    const {content} = pageContent;
                    let arr = []
                    content.filter(con => con?.data?.target?.sys?.contentType?.sys?.id === 'profile').forEach(el => arr.push(el.data.target))
                    setProfiles(arr)
                    typeof entry === 'undefined' ? setLoading(true) : setLoading(false);
                })
        }

    })

    if (loading)
        return (<ProfileSection className={`section is-flex is-flex-direction-column`}>
                <ContainerProfile className=''>
                </ContainerProfile>
            </ProfileSection>
        )

    const contactLinks = entry.contactLinks ? entry.contactLinks : {};

    const options = {
        renderMark: {
            [MARKS.BOLD]: text => <b>{text}</b>,
        },
        renderNode: {
            [BLOCKS.PARAGRAPH]: (node, children) => <ArticleItemText>{children}</ArticleItemText>,
            [BLOCKS.HEADING_2]: (node, children) => <ArticleItemTitle>{children}</ArticleItemTitle>,
            [BLOCKS.HEADING_3]: (node, children) => <ArticleItemBlockQuote>{children}</ArticleItemBlockQuote>,
            [BLOCKS.EMBEDDED_ASSET]: (node /*, children**/) => {
                // render the EMBEDDED_ASSET as you need
                return (
                    <ArticleImage>
                        <img
                            src={`https://${node.data.target.fields.file.url}`}
                            height={node.data.target.fields.file.details.image.height}

                            width='100%'
                            alt={node.data.target.fields.description}
                        />
                    </ArticleImage>
                );
            },
            [BLOCKS.UL_LIST]: (node, children) => {
                return(
                    <UnOrderedList>{children}</UnOrderedList>
                )
            },
            [BLOCKS.EMBEDDED_ENTRY]: (node) => {
                return(
                    <ImageParagraph
                        id={node.data.target.sys.id}
                        fields={node.data.target.fields}/>
                )
            }
        },
    }

    const onMouseEnterTwitter = () => {
        setTwitterIconColor(dark)
    }
    const onMouseLeaveTwitter = () => {
        setTwitterIconColor(norm)
    }
    const onMouseEnterLinkedIn = () => {
        setLinkedinIconColor(dark)
    }
    const onMouseLeaveLinkedIn = () => {
        setLinkedinIconColor(norm)
    }
    const onMouseEnterWeb = () => {
        setWebIconColor(dark)
    }
    const onMouseLeaveWeb = () => {
        setWebIconColor(norm)
    }
    const index = profiles.findIndex(item => {
        return (item.fields.slug === slug)
    })

    const next_prev = (index) => {
        if (profiles && profiles.length > 0) {
            let last = profiles.length - 1
            if (index === last) {
                return [profiles[last - 1].fields.slug, profiles[0].fields.slug, profiles[last - 1].fields.profileName, profiles[0].fields.profileName]
            } else if (index === 0) {
                return [profiles[last].fields.slug, profiles[1].fields.slug, profiles[last].fields.profileName, profiles[1].fields.profileName]
            } else {
                return [profiles[index - 1]?.fields.slug, profiles[index + 1].fields.slug, profiles[index - 1]?.fields.profileName, profiles[index + 1].fields.profileName]
            }
        } else {
            return '/profiles'
        }

    }

    return (
        <ProfileSection className={`section is-flex is-flex-direction-column`}>
            <ContainerProfile className=''>
                {
                    <>
                        <TitleSection>
                            <Image src={entry?.profilePhoto?.fields?.file.url}/>
                            <h1 style={{fontSize: 48}} className="title">{entry?.profileName}</h1>
                            <Logos>
                                <a href={contactLinks.LinkedIn} onMouseEnter={onMouseEnterTwitter}
                                   onMouseLeave={onMouseLeaveTwitter}>
                                    <LinkedInLogo
                                        class="logo"
                                        fill={twitterIconColor}/>
                                </a>
                                <a href={contactLinks?.Twitter} style={{padding: '0 1em'}}
                                   onMouseEnter={onMouseEnterLinkedIn}
                                   onMouseLeave={onMouseLeaveLinkedIn}>
                                    <TwitterLogo
                                        class="logo"
                                        fill={linkedinIconColor}/>
                                </a>
                                <a style={{color: webIconColor}} href={contactLinks?.Website}
                                   onMouseEnter={onMouseEnterWeb}
                                   onMouseLeave={onMouseLeaveWeb}>
                                    <WebLogo/>
                                </a>
                            </Logos>
                            {entry?.profileCompanyLogo &&
                                <a href={entry?.organizationUrl} target="_blank" rel="noreferrer">
                                    <Logo src={entry?.profileCompanyLogo?.fields?.file.url}/>
                                </a>}
                        </TitleSection>

                        <QuickInfoSection>
                            <hr style={{height: 3, backgroundColor: "#4c9caf"}}/>
                            {!entry?.keyStatsHide && entry?.keyStats && <table border="0" style={{borderWidth: 0, textDecoration: 'none'}}>
                                <tbody>
                                {
                                    entry?.keyStats.map((stat, index) => {
                                        return (<tr style={{}} key={index}>
                                            <td style={{padding: 10}}>
                                                {stat?.name && <b>{stat?.name}</b>}
                                            </td>
                                            <td style={{padding: 10, width: '55%', textAlign: 'end'}}>
                                                {stat?.value && <span>{stat?.value} </span>}
                                            </td>
                                        </tr>)
                                    })
                                }
                                </tbody>
                            </table>}
                        </QuickInfoSection>
                        <ArticleSection>
                            <div>
                                {documentToReactComponents(entry?.profileArticle, options)}
                            </div>
                        </ArticleSection>
                        <NextPrevArticle>
                            <a href={next_prev(index)[0]}>
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor"
                                     className="bi bi-arrow-left-short" viewBox="0 0 16 16">
                                    <path fillRule="evenodd"
                                          d="M12 8a.5.5 0 0 1-.5.5H5.707l2.147 2.146a.5.5 0 0 1-.708.708l-3-3a.5.5 0 0 1 0-.708l3-3a.5.5 0 1 1 .708.708L5.707 7.5H11.5a.5.5 0 0 1 .5.5z"/>
                                </svg>
                                {next_prev(index)[2]}
                            </a>
                            <a href={next_prev(index)[1]}>
                                {next_prev(index)[3]}
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor"
                                     className="bi bi-arrow-right-short" viewBox="0 0 16 16">
                                    <path fillRule="evenodd"
                                          d="M4 8a.5.5 0 0 1 .5-.5h5.793L8.146 5.354a.5.5 0 1 1 .708-.708l3 3a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708-.708L10.293 8.5H4.5A.5.5 0 0 1 4 8z"/>
                                </svg>
                            </a>
                        </NextPrevArticle>
                    </>
                }
            </ContainerProfile>
        </ProfileSection>
    )
}



const ProfileSection = styled.section`
  background-color: white;
  justify-content: center;
  max-width: 100%;
  p {
    margin-bottom: 15px;
  }


`
const ContainerProfile = styled.section`
  min-height: 1000px;
  margin: auto;
  @media (min-width: 768px) {
    max-width: 67vw;
  }
  @media (max-width: 576px){
    margin: 0;
  }
`


const TitleSection = styled.section`
  text-align: center;
  margin-bottom: 50px;
  margin-top: 100px;
`


const QuickInfoSection = styled.section`
  justify-content: center;
  align-items: center;

  @media (max-width: 500px) {
    margin: 0;
  }
  //    max-width: 900px; 
  font-family: "Merriweather Sans", sans-serif;

`
const ArticleSection = styled.section`
  justify-content: center;
  align-items: center;
  //    max-width: 900px;
  margin-top: 75px;

`
const ArticleItemTitle = styled.h1`
  font-weight: bold;
  margin: 30px 150px;
  font-family: "Merriweather Sans", sans-serif;
  font-size: 24px;
  @media (max-width: 768px) {
    margin: 30px 10px;
  }

`
const ArticleItemText = styled.p`
  margin: 30px 150px;
  font-family: "Merriweather", sans-serif;
  @media (max-width: 768px) {
    margin: 30px 10px;
  }
`
const ArticleItemBlockQuote = styled.p`
  font-size: 28px;
  margin: 30px 150px;
  font-family: "Merriweather Sans", sans-serif;
  text-align: left;
  @media (max-width: 768px) {
    margin: 30px 10px;
  }
`


const Image = styled.img`
  max-width: 380px;
  min-width: 380px;
  border-radius: 10px;
  @media (max-width: 768px) {
    max-width: 300px;
    min-width: 300px;
  }
`

const Logo = styled.img`
  max-width: 500px;
  max-height: 80px;
  @media (max-width: 768px) {
    max-width: 300px;
    min-width: 50px;
  }

`
const NextPrevArticle = styled.div`
  margin: 30px 130px;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  @media (max-width: 768px){
    margin: 30px 0;
  }

  a {
    color: #BD5E8C;
    display: flex;
    flex-direction: row;
    align-items: center;
  }
`
const UnOrderedList = styled.ul`
  li {
    list-style-type: disc;
    margin: 30px 150px;
    p {
      margin: 0 !important;
    }
    @media (max-width: 768px) {
      margin: 30px 10px;
    }
  }
`
const ArticleImage = styled.div`
  text-align: center;
  margin: 50px 190px 0;
  @media (max-width: 768px) {
    margin: 30px 50px 0;
  }
`
