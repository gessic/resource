import React from "react";
import styled from "styled-components";


const BannerWrapper = styled.div`
  background-image: url(${props => props.image});
  background-size: cover;
  height: 20em;
  margin-bottom: ${props => props.marginBottom ? '5em' : null};
  background-color: ${props => props.backgroundColor ? props.backgroundColor : null};
  @media (max-width: 1000px) {
    height: 350px;
  }
  @media(max-width: 576px){
    height: 425px;
    h1{
      text-align: center;
    }
  }
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;

  h1 {
    color: white;
    padding: 0 2em;
  }

  h2 {
    color: white;
    padding: 0 2em;
    text-align: center;
    max-width: 900px;
    line-height: 1.5;
  }

  img {
    margin-top: -0.25em;
  }

  a:hover {
    color: white;
  }
`

const CTA = styled.a`
  padding: 1em 4em;
  background-color: #3592A4;
  color: white;
  font-weight: bold;
  font-size: 24px;
  border-radius: 10px;
  ${props => props.border ? 'border: 2px solid #9AE368; ' : '' };
  @media (max-width: 1000px) {
    padding: 1.5em 1em;
  }

`
//Banner Component Used across different Pages
const Banner = (props) => {
    const {title, subtitle, cta, image, backgroundColor, topMargin, showImage, customCta, marginBottom} = props;
    return (
        <>
            <BannerWrapper style={{marginTop: topMargin}} image={showImage ? `https:${image?.fields?.file?.url}` : ''}
                           backgroundColor={backgroundColor} marginBottom={marginBottom}>
                {title && <h1 className="title">{title}</h1>}
                {subtitle && <h2 className="subtitle">{subtitle}</h2>}
                {cta && <CTA href={cta?.fields?.url} border={customCta}>{cta?.fields?.name}</CTA>}
            </BannerWrapper>
        </>
    )
}

Banner.propTypes = {
};

export default Banner;
