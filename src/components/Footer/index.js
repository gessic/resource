import React, {useState, useEffect} from "react"
import styled from "styled-components"
import footerLogo from '../../images/footerLogo.png'

export default function Footer() {
    const [scroll, setScroll] = useState(1)
    useEffect(() => {
        const onScroll = () => {
            const scrollCheck = window.scrollY < 200
            if (scrollCheck !== scroll) {
                setScroll(scrollCheck)
            }
        }
        document.addEventListener("scroll", onScroll)
        return () => {
            document.removeEventListener("scroll", onScroll)
        }
    }, [scroll, setScroll])

    return (
        <FooterSection className={`is-flex-tablet is-justify-content-space-between  is-align-items-center`}>
            <FooterLogo>
                <a onClick={() => window['scrollTo']({top: 0})}>
                    <img src={footerLogo} style={{maxWidth: 300, maxHeight: 125}} alt=''/>
                </a>
            </FooterLogo>
            <div>
                <ButtonToTop
                    className={`button is-light is-fixed ${scroll ? "is-hidden" : ""}`}
                    onClick={() => window['scrollTo']({top: 0, behavior: 'smooth'})}
                >
                    <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" fill="currentColor"
                         className="bi bi-chevron-up" viewBox="0 0 16 16">
                        <path fillRule="evenodd"
                              d="M7.646 4.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1-.708.708L8 5.707l-5.646 5.647a.5.5 0 0 1-.708-.708l6-6z"/>
                    </svg>
                </ButtonToTop>
            </div>

        </FooterSection>
    )
}

const FooterSection = styled.section`
  border-top: 1px solid #ccc;
  background-color: #E3EFF1;
  padding-top: 20px;
  padding-right: 100px;
  padding-left: 100px;
  width: 100%;
  @media (max-width: 576px) {
    padding: 20px 0 0;

  }
`
const FooterLogo = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
`
const ButtonToTop = styled.button`
  position: fixed;
  right: 10px;
  bottom: 10px;
  z-index: 99;
  transition: all 0.5s;
  box-shadow: rgba(50, 50, 93, 0.25) 0 2px 5px -1px, rgba(0, 0, 0, 0.3) 0px 1px 3px -1px;
  height: 3.25rem;
  width: 3.25rem;
  border-radius: 50%;
  border: 1px solid black;
  @media (max-width: 768px) {
    && span {
      display: none
    }
  }
`;
