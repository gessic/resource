import React from "react"
import styled from "styled-components"
import RichTextRenderer from "../../renderer/RichTextRenderer";

export default function InfoItem({content, color}) {
    return (
        <InfoItemWrapper className={'column p-6 is-flex  is-flex-direction-column'}>
            <InfoItemLogoWrap className={'is-flex is-align-items-center is-flex-direction-column is-justify-content-center'}>
                <InfoItemLogo src={`http:${content.image.fields.file.url}`} alt={content.image.fields.description} />
            </InfoItemLogoWrap>
            <div style={{color, paddingBottom: 10, textAlign: 'left'}}>
                {RichTextRenderer.render(content.content)}
            </div>
        </InfoItemWrapper>
    )
}

const InfoItemLogoWrap = styled.div`
    min-height: 140px;
`
const InfoItemLogo = styled.img`
    margin: 0 auto;
  max-height: 100px;
`
const InfoItemWrapper = styled.section`
  p {
      margin-bottom: 1em;
  }
`
