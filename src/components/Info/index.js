import React from 'react';
import styled from 'styled-components';
import InfoItem from './InfoItem';
import RichTextRenderer from "../../renderer/RichTextRenderer";
import ScrollAnimation from 'react-animate-on-scroll'
import PropTypes from "prop-types";


const InfoWrapper = styled.div`
  background-color: ${props => props.backgroundColor};
  padding: 100px 0 0;
  @media (max-width: 768px) {
    padding-top: 75px;
    padding-bottom: 0;

  }
  @media (max-width: 992px) {
    padding: 100px 2em 0;
  }
`;

const Title = styled.h2`
  font-size: 42px;
  font-weight: bold;
  color: ${props => props.color};
  text-align: ${props => props.align};
  margin: auto auto 40px;
  max-width: 900px;

  @media (max-width: 768px) {
    margin-bottom: 20px;
  }
`

const TextContent = styled.div`
  color: ${props => props.color};
  max-width: 900px;
  margin: auto;

  p {
    margin-bottom: 1em;
  }
`
const CTA = styled.div`
  max-width: 900px;
  margin: auto;
`
const TextLink = styled.div`
  font-weight: bold;
  color: #3592A5;
  font-size: 1.2rem;
  text-decoration: underline 3px;
  //border-bottom: 3px solid #3592A5;
  //width: fit-content;
`
const Button = styled.button`
  margin-top: 3rem;
  background-color: #3592A5;
  color: white;
  height: 60px;
  width: 250px;
  border-radius: 10px;
  border: 0;
  text-transform: uppercase;
  font-weight: bold;
  font-size: 1.1rem;
  cursor: pointer;
`

const Info = ({
                  title,
                  content,
                  textContent,
                  backgroundColor,
                  textColor,
                  fadeIn,
                  whiteBackground,
                  dangerous,
                  titleWithStyle,
                  callToAction
              }) => {
    return (
        <InfoWrapper id={title?.fields?.id} backgroundColor={whiteBackground ? 'white' : backgroundColor}
                     key={Math.random()}>
            <div>
                {fadeIn ?
                    <ScrollAnimation animateOnce={true} animateIn="animate__fadeInUp">
                        {dangerous ?
                            <Title align='left' color={textColor} dangerouslySetInnerHTML={{__html: titleWithStyle}}/>
                            :
                            <Title align={title?.fields?.align} color={textColor}>
                                {title?.fields?.title}
                            </Title>}
                    </ScrollAnimation>
                    :
                    <Title align={title?.fields?.align} color={textColor}>
                        {title?.fields?.title}
                    </Title>}

                <ScrollAnimation animateOnce={true} animateIn="animate__fadeInUp" delay={500}>
                    <TextContent>
                        {textContent && RichTextRenderer.render(textContent)}
                    </TextContent>
                    {callToAction && <CTA>
                        <TextLink>
                            {callToAction?.fields?.title}
                        </TextLink>
                        <Button>
                            {callToAction?.fields?.name}
                        </Button>
                    </CTA>}
                </ScrollAnimation>
                <div className={'container'}>
                    <div className={'columns is-flex-tablet  is-justify-content-space-around '}>
                        {content && content.map(contentItem => <InfoItem color={textColor} key={contentItem?.sys?.id}
                                                                         content={contentItem?.fields}/>)}
                    </div>
                </div>
            </div>
        </InfoWrapper>
    );
};

Info.propTypes = {
    props: PropTypes.object.isRequired
};

export default Info;
