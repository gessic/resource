import React from "react"
import AccordionItem from "./AccordionItem"
import styled from 'styled-components'


const ESOs = [
    {
        title: "What’s the catch?",
        subTitle: "No catch! You just have to be selected to join from a competitive pool of applicants"
    },

    {
        title: "What do I get?",
        subTitle: "Hands-on training on organization management techniques, best practices in program design and delivery, peer connections to other leaders across the country… Oh, and money. Participants in Resource receive $20,000 cash to invest in your organization’s growth."
    },

    {
        title: "How do I apply to join the program?",
        subTitle: "Applications are now being accepted for the second iteration of the program. Check back soon for the application link.",
        emailLink: <a href="https://eso.abaca.app/entrepreneurs/program/?a=resource2">Apply Today</a>
    },

]

const foundationsAndDonors = [
    {
        title: "I’m a representative of a foundation or funder interested in getting to know the ESOs you’re working with. How do I learn more about that?",
        subTitle: "We are pleased to offer opportunities to engage the ESOs directly. Please reach out to Dahlia Joseph at ",
        emailLink: <a href="mailto: dahlia.joseph@vilcap.com">dahlia.joseph@vilcap.com</a>
    },

    {
        title: "I’m a representative of a foundation. How do I support this work?",
        subTitle: "We are currently developing a core group of funding partners to take this initiative forward. Please reach out directly to Dustin Shay at ",
        emailLink: <a href="mailto: dustin@vilcap.com">dustin.shay@vilcap.com</a>
    },

    {
        title: "I want to support this work, but I’m not a foundation or funder representative. I’ve got like $20. Can I help?",
        subTitle: "Sure! Click the ‘donate’ button at the top of the page, and we’ll put your dollars to work creating a more equitable entrepreneurial ecosystem."
    },
]


const others = [
    {
        title: "I’m a service provider looking to engage with the ESOs, can you help me get in touch?",
        subTitle: "Sure! Send Dahlia Joseph a note at ",
        emailLink: <a href="mailto: dahlia.joseph@vilcap.com">dahlia.joseph@vilcap.com</a>
    },
    {
        title: "I’m an investor and I want access to the pipeline of companies coming through these accelerators. How do I do that?",
        subTitle: "We’ll be offering a more automated approach on this front soon. For now, email Dahlia Joseph at ",
        emailLink: <a href="mailto: dahlia.joseph@vilcap.com">dahlia.joseph@vilcap.com</a>
    },
    {
        title: "I’m with the press and think this is an exciting story. How can I find out more?",
        subTitle: "Great! Please reach out to our comms team lead Ben Wrobel at ",
        emailLink: <a href="mailto: ben.wrobel@vilcap.com">ben.wrobel@vilcap.com</a>
    },

]

const sectionTitles = {
    title: "How to Get Involved",
    subTitle: "If your interested in joining our efforts, there may be opportunities for you to participate in different ways. Reach out to dahlia@vilcap.com or expand the sections relevant to you below."
}

const FAQTitles = {
    ESOs: "Entrepreneur Support Organizations",
    foundationsAndDonors: "Foundations and Donors",
    others: "Others"
}


export default function GetInvolved() {

    return (
        <Accordion>

            <OuterSection id="getInvolved"
                          className={`section is-flex  is-justify-content-center is-align-items-center`}>
                <div style={{textAlign: 'center', maxWidth: 700}}>
                    <h1 className={"title"}> {sectionTitles.title} </h1>
                    <p>
                        Want to get involved? Learn more below, or reach out to
                        <a href="mailto:dahlia.joseph@vilcap.com">dahlia.joseph@vilcap.com</a>
                    </p>
                </div>

            </OuterSection>

            <AccordionItem items={ESOs} title={FAQTitles.ESOs} color={'#E3EFF1'}/>
            <AccordionItem items={foundationsAndDonors} title={FAQTitles.foundationsAndDonors} color={'#E3EFF1'}/>
            <AccordionItem items={others} title={FAQTitles.others} color={'#E3EFF1'}/>

        </Accordion>
    )
}

const Accordion = styled.div`
  margin: auto auto 100px;
  max-width: 1024px;
  @media (max-width: 768px) {
    margin-left: 0;
    margin-right: 0;
  }
`

const OuterSection = styled.div`
  padding: 100px 0;
  @media (max-width: 768px) {
    padding: 50px 25px;
  }
`;
