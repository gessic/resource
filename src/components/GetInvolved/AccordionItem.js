import React, {useState} from "react"
import AngleDownIcon from '../../images/icons/angle-down-light.svg'
import MinusIcon from '../../images/icons/minus.svg'
import styled from 'styled-components'

export default function AccordionItem(props) {

    const [open, setOpen] = useState(false)

    return (
        <section style={{backgroundColor: props.color,}}>
            <a onClick={() => {
                setOpen(!open)
            }}>
                <AccordionHeaderRow className="is-flex is-justify-content-space-between">
                    <AccordionHeaderRowH1>  {props.title} </AccordionHeaderRowH1>
                    {open ? <img alt='' src={MinusIcon} className='icon'/> :
                        <img alt='' src={AngleDownIcon} className='icon'/>}
                </AccordionHeaderRow>
            </a>
            {open &&
                <AccordionSection>
                    {props.items.map(item => {
                        return (
                            <div className='pl-4 pr-4 pt-6 pb-6 ml-5'>
                                <h3 className='accordion-header'>
                                    {item.title}
                                </h3>
                                <p className='p-5'>
                                    {item.subTitle} {item.emailLink ? item.emailLink : null}
                                </p>
                            </div>
                        )
                    })}
                </AccordionSection>
            }

        </section>
    )
}

const AccordionHeaderRow = styled.div`
  padding: 20px 40px;
  border-bottom: 1px solid #fff;

  .icon {
    position: relative;
    top: 15px;
  }
`

const AccordionHeaderRowH1 = styled.h1`
  color: #3d424c;
  font-weight: bold;
  font-size: 22px;
  display: flex;
  align-items: center;
`

const AccordionSection = styled.section`
  background-color: #fff;

  .accordion-header {
    font-weight: 600;
    font-size: 20px
  }
`
