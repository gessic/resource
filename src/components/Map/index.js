import React, {useEffect, useState} from "react";
import {
    ComposableMap,
    Geographies,
    Geography,
    Marker,
    Annotation
} from "react-simple-maps";
import CohortList from '../Cohorts/CohortList.json'
import './map.css'

const geoUrl = "https://cdn.jsdelivr.net/npm/us-atlas@3/states-10m.json";


const Map = () => {
    const [cohortID, setCohortID] = useState("")
    const [isSafari, setIsSafari] = useState(false)


    useEffect(() => {
        let ua = navigator.userAgent.toLowerCase();
        if (ua.indexOf('safari') !== -1) {
            if (ua.indexOf('chrome') > -1) {
            } else {
                setIsSafari(true)
            }
        } else {
            // console.log("firefox")
        }
    }, [isSafari])


    return (
        <ComposableMap
            style={{maxWidth: 900}}
            projection="geoAlbersUsa">
            <Geographies geography={geoUrl}>
                {({geographies}) => (
                    <>
                        {geographies.map((geo) => (
                            <Geography
                                key={geo.rsmKey}
                                stroke="rgba(255, 255, 255, .2)"
                                geography={geo}
                                fill="#3592A5"
                                style={{
                                    default: {outline: "none"},
                                    hover: {outline: "none"},
                                    pressed: {outline: "none"},

                                }}
                            />
                        ))}

                    </>
                )}

            </Geographies>
            {CohortList.map((cohort, index) => {
                return (
                    <React.Fragment key={index}>
                        <a href={cohort.url} rel="noreferrer" target="_blank">
                            <Marker href={cohort.url}
                                    style={{zIndex: 2}}
                                    coordinates={[cohort.long, cohort.lat]}
                                    onMouseEnter={() => {
                                        setCohortID(cohort.id)
                                    }}
                                    onTouchStart={() => {
                                        setCohortID(cohort.id)
                                    }}
                                    onMouseLeave={() => {
                                        setCohortID("");
                                    }}
                            >
                                {cohortID !== cohort.id ?
                                    <circle r={5} fill="#D8ECC8"/> :
                                    <g id="svg_3">
                                        <ellipse strokeWidth="0" ry="9" rx="8.66667" id="svg_2" opacity="undefined"
                                                 stroke="#D8ECC8" fill="#e3eff1"/>
                                        <ellipse ry="6" rx="6" id="svg_1" opacity="undefined" stroke="#D8ECC8"
                                                 fill="#D8ECC8"/>
                                    </g>
                                }
                            </Marker>
                        </a>
                        <Annotation
                            style={{zIndex: 1}}
                            subject={[cohort.long, cohort.lat]}
                            dx={cohort.dx}
                            dy={cohort.dy}
                            connectorProps={{
                                stroke: "#FF5533",
                                strokeWidth: 0,
                                strokeLinecap: "curved",
                            }}
                        >
                            {cohortID === cohort.id ? <svg>
                                <defs>
                                    <filter id="svg_1_blur">
                                        <feGaussianBlur stdDeviation="1" in="SourceGraphic"/>
                                    </filter>
                                </defs>
                                <g id="Layer_1">
                                    <title>Ecosystem Locations Across the Country</title>
                                    <image style={{pointerEvents: 'none'}} href={cohort.base64 ? cohort.base64 : null}
                                           id="svg_5" height="70" width="149" y="4" x="9"/>

                                </g>
                            </svg> : null}
                            <svg className={cohort.class}>
                                <defs>
                                    <filter id="svg_1_blur">
                                        <feGaussianBlur stdDeviation="1" in="SourceGraphic"/>
                                    </filter>
                                </defs>
                                <g id="Layer_1">
                                    <title>Ecosystem Locations Across the Country</title>
                                    <image style={{pointerEvents: 'none'}} href={cohort.base64 ? cohort.base64 : null}
                                           id="svg_5" height="70" width="149" y="4" x="9"/>
                                </g>
                            </svg>
                        </Annotation>
                    </React.Fragment>
                )
            })}

        </ComposableMap>
    );
};

export default Map;
