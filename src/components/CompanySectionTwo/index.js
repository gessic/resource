import React from 'react';
import styled from 'styled-components';


const CompanySection = ({sponsors, urls, alts, oneRow, twoRow}) => {
    return (
        <CompanySectionBlock className='p-3'>
            {twoRow && <div style={{marginRight: 40, marginLeft: 40, textAlign: 'center', marginBottom: 70}}>
                <a target="_blank" rel="noreferrer" href={urls[3]}> <CompanyLogo src={sponsors[3]} alt={alts[3]}/> </a>
            </div>}
            <Section className="is-flex">
                <div className="columns">
                    <div style={{marginRight: 40, marginLeft: 40, marginBottom: 20}} className="column">
                        <a target="_blank" rel="noreferrer" href={urls[0]}> <CompanyLogo src={sponsors[0]}
                                                                                         alt={alts[0]}/> </a>
                    </div>
                    z
                    <div style={{marginRight: 40, marginLeft: 40, marginBottom: 20}} className="column">
                        <a target="_blank" rel="noreferrer" href={urls[1]}> <CompanyLogo src={sponsors[1]}
                                                                                         alt={alts[1]}/> </a>
                    </div>

                    <div style={{marginRight: 40, marginLeft: 40, marginBottom: 20}} className="column">
                        <a target="_blank" rel="noreferrer" href={urls[2]}> <CompanyLogo src={sponsors[2]}
                                                                                         alt={alts[2]}/></a>
                    </div>
                    {oneRow && <div style={{marginRight: 40, marginLeft: 40, marginBottom: 20}} className="column">
                        <a target="_blank" rel="noreferrer" href={urls[3]}> <CompanyLogo src={sponsors[3]}
                                                                                         alt={alts[3]}/> </a>
                    </div>}
                </div>
            </Section>

        </CompanySectionBlock>

    );
}

const CompanySectionBlock = styled.section`
  position: relative;
  margin-bottom: 200px;
  @media (max-width: 768px) {
    margin-bottom: 50px;
  }
`

const CompanyLogo = styled.img`
  margin: 0 auto;
  max-height: 50px;
  @media (max-width: 768px) {
    max-width: 400px
  }
`

const Section = styled.div`
  display: flex;
  margin: 0 100px;

  justify-content: space-around;
  align-items: center;
  align-content: center;
  position: relative;
  @media (max-width: 768px) {
    margin: auto;
  }
`;

export default CompanySection;
