import React, {useEffect, useState} from "react";
import styled from "styled-components";
import RichTextRenderer from "../../renderer/RichTextRenderer";
import "../../../node_modules/video-react/dist/video-react.css"; // import css
import {Player} from 'video-react'
import {Line} from "../ProfileItem";
import './video.css'
import PropTypes from "prop-types";


const Wrapper = styled.div`
  //box-shadow: 5px 5px 15px #ddd, -5px -5px 15px #ddd;
  //  max-width: 60%;
  width: 75%;
  //width: 100%;
  margin: 2em auto;
  border-radius: 0.5em;
  display: flex;
  flex-direction: column;
  //padding: 1em;
  @media (max-width: 768px) {
    //max-width: 400px;
  }
  @media (max-width: 576px) {
    //max-width: 280px;
    margin: 3em 0;
  }
`;
const TextArea = styled.section`
  padding: 1.5em 0;
  width: 80%;

  p {

  }

  p:nth-child(2) {
    //padding-top: 1em;
  }
`;
const VideoName = styled.div`
  padding-top: calc(1.61rem + 1em);
  padding-bottom: calc(1em + 28px);

  h1 {
    color: #62A3B0;
    font-size: 1.4rem;
  }
`;
function debounce(fn, ms) {
    let timer
    return _ => {
        clearTimeout(timer)
        timer = setTimeout(_ => {
            timer = null
            fn.apply(this, arguments)
        }, ms)
    };
}

//Video Player component
const Video = (props) => {
    const {videoName, videoLink, videoDescription, videoPoster, fullWidth, slug} = props
    const [height, setHeight] = useState(0)
    const [loaded, setLoaded] = useState(false)
    useEffect(() => {
        let imgContainer = document.getElementsByClassName('imageContainer')[1]
        if (!loaded) {
            setTimeout(_ => {
                setHeight(imgContainer.clientHeight)
            }, 200)
            setLoaded(true)
        }
        const debouncedHandleResize = debounce(function handleResize() {
            setHeight(imgContainer.clientHeight)
        }, 500)
        const handleLoad = () => {
            setHeight(imgContainer.clientHeight)
        }

        window.addEventListener('resize', debouncedHandleResize)
        imgContainer.addEventListener('load', handleLoad)
        //imgContainer[0].addEventListener('resize', debouncedHandleResize)

        return () => {
            window.removeEventListener('resize', debouncedHandleResize)
            imgContainer.removeEventListener('load', handleLoad)
            //imgContainer[0].addEventListener('resize', debouncedHandleResize)
        }
    }, [loaded])
    const calcWidth = (height) => {
        //console.log((height * 16) / 9, height)
        return (height * 16) / 9
    }
    return (
        <>
            <Wrapper id={slug} width={fullWidth ? '75%' : `${calcWidth(height)}px`}>
                <Player playsInline poster={videoPoster.fields.file.url} src={videoLink}/>

                <VideoName>
                    <h1 className='title'>
                        {videoName}
                    </h1>
                </VideoName>
                <Line/>
                <TextArea>
                    {RichTextRenderer.render(videoDescription)}
                </TextArea>
            </Wrapper>
        </>
    )
}
Video.proptypes = {
    props: PropTypes.object.isRequired
}

export default Video
