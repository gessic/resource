import React from "react";
import {graphql, useStaticQuery} from "gatsby";
import {Helmet} from "react-helmet";
import PropTypes from "prop-types";
import favicon from '../../images/favicon.svg'


const Seo = ({description, title, lang, meta}) => {
    const {site} = useStaticQuery(
        graphql`
            query{
                site {
                    siteMetadata {
                        title
                        description
                        url
                        image
                    }
                }
            }
        `
    )

    const metaDescription = description || site.siteMetadata.description

    return (
        <Helmet
            htmlAttributes={{
                lang,
            }}
            title={title}

            meta={[
                {
                    name: `description`,
                    content: metaDescription,
                },
                {
                    property: `og:title`,
                    content: title
                },
                {
                    property: `og:description`,
                    content: metaDescription
                },
                {
                    property: `og:type`,
                    content: `website`
                },
                {
                    property: `og:image`,
                    content: site.siteMetadata.image
                },
                {
                    property: `og:url`,
                    content: site.siteMetadata.url
                },
                {
                    name: `twitter:card`,
                    content: `summary_large_image`
                },
                {
                    name: `twitter:title`,
                    content: title
                },
                {
                    name: `twitter:description`,
                    content: metaDescription
                },
                {
                    name: `twitter:image`,
                    content: site.siteMetadata.image
                },
            ].concat(meta)}
            link={[
                {
                    rel: "canonical",
                    href: site.siteMetadata.url
                },
                {
                    rel: "icon",
                    href: `${favicon}`
                }
            ]}

        />
    )
}

Seo.defaultProps = {
    lang: `en`,
    meta: [],
    description: ``
}

Seo.propTypes = {
    description: PropTypes.string,
    lang: PropTypes.string,
    meta: PropTypes.arrayOf(PropTypes.object),
    title: PropTypes.string.isRequired
}

export default Seo
