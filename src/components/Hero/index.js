import React from "react";
import styled from "styled-components";
import dottedMap from './dottedmap.svg'
import PropTypes from 'prop-types';


const HeroSection = styled.section`
  background-color: #3592A5;
  background-position: 50% 50%;
  background-size: 100% auto;
  background-repeat: no-repeat;
  height: 900px;
  padding: 0 50px;
  margin: 0 auto;
  @media (min-width: 577px) and (max-width: 992px) {
    height: 1500px;
  }
  @media (max-width: 576px) {
    height: 1300px;
    padding: 0 0;
  }
  @media (max-width: 300px) {
    height: 800px;
  }
  @media (min-width: 301px) and (max-width: 450px) {
    height: 1000px;
  }
`
const HeroDiv = styled.div`
  background-image: url(${dottedMap});
  background-position: 50% 50%;
  background-size: 90% auto;
  background-repeat: no-repeat;
  height: 100%;
  width: auto;
  display: flex;
  flex-direction: row;
  max-width: 1315px;
  margin: 0 auto;
  @media (min-width: 1450px) {
    background-size: 50%, auto;
    background-position: center;
  }
  @media (max-width: 992px) {
    flex-direction: column;
    background-size: 110% auto;
  }
`
const HeroTitle = styled.h1`
  font-weight: bold;
  color: white;
  font-size: 48px;
  text-align: start;
  padding-bottom: 20px;
  max-width: 500px;
  @media (max-width: 768px) {
    font-size: 32px;
    line-height: 33px;
  }
`

const HeroCta = styled.a`
  background-color: transparent;
  border: 2px solid #9AE368;
  border-radius: 10px;
  padding: .3em 1em;
  color: white;
  margin-top: 1rem;

  &:hover {
    color: white;
  }
`
const HeroHalf = styled.div`
  width: 50%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  height: 100%;
  ${props => props.left && ' padding-left: 6rem; align-items: start;'};
  ${props => props.right && 'display: block; align-items: center;'};
  @media (max-width: 992px) {
    width: 100%;
    padding-left: 0;
  }
  @media (min-width: 769px) and (max-width: 992px) {
    padding-left: 0;
    margin-left: 3.5rem;
  }
  @media (max-width: 576px) {
    ${props => props.left && ' padding: 0 50px;'};
    ${props => props.right && 'padding: 0;'};
  }
`
const ImageContainer = styled.div`
  position: relative;
  top: 25%;
  width: 100%;
  @media (max-width: 992px) {
    top: 0;
    margin-top: -5em;
  }
  @media (max-width: 576px) {
    left: -1.4em;
  }
  @media (max-width: 300px) {
    left: -3.4em;
  }
`
const ImageDiv = styled.div`
  position: absolute;
  top: ${props => props.top}rem;
  left: ${props => props.left}rem;
  @media (max-width: 576px) {
    width: 50%;
    top: ${props => props.topSm}rem;
    left: ${props => props.leftSm}rem;
  }
  @media (max-width: 349px) {
    width: 50%;
    top: ${props => props.topXsm}rem;
    left: ${props => props.leftXsm}rem;
  }
  @media (min-width: 577px) and (max-width: 767px) {
    width: 60%;
    top: ${props => props.topMd}rem;
    left: ${props => props.leftMd}rem;
  }
  @media (min-width: 992px) and (max-width: 1200px) {
    top: ${props => props.topLg}rem;
    left: ${props => props.leftLg}rem;
  }
  @media (min-width: 350px) and (max-width: 450px) {
    width: 50%;
    top: ${props => props.topMsm}rem;
    left: ${props => props.leftMsm}rem;
  }
`
const Image = styled.img``


//Hero Section with animations' on Homepage
function Hero(props) {
    console.log('hero props', props)
    const {title, button, images} = props
    return (
        <>
            <HeroSection>
                <HeroDiv>
                    <HeroHalf left={true}>
                        <HeroTitle>
                            {title}
                        </HeroTitle>
                        <HeroCta href='#about'>
                            {button}
                        </HeroCta>
                    </HeroHalf>
                    <HeroHalf right={true}>
                        <ImageContainer>
                            {images && images.map((img, index) => {
                                const {
                                    image,
                                    delay,
                                    left,
                                    leftLg,
                                    leftMd,
                                    leftMdSm,
                                    leftSm,
                                    leftXSm,
                                    top,
                                    topLg,
                                    topMSm,
                                    topMd,
                                    topSm,
                                    topXSm
                                    // eslint-disable-next-line no-unsafe-optional-chaining
                                } = img?.fields
                                return (
                                    <ImageDiv id={index + 1} key={index} top={top} topSm={topSm}
                                              leftSm={leftSm}
                                              left={left}
                                              topMd={topMd}
                                              leftMd={leftMd}
                                              leftLg={leftLg}
                                              topLg={topLg}
                                              topXsm={topXSm}
                                              leftXsm={leftXSm}
                                              topMsm={topMSm}
                                              leftMsm={leftMdSm}
                                    >
                                        <div
                                            className={`animate__animated animate__flipInX ${delay}`}
                                            animateIn='animate__flipInX'
                                            style={{width: '60%'}}
                                            animateOnce={true}
                                        >
                                            <Image src={image?.fields?.file?.url}/>
                                        </div>
                                    </ImageDiv>
                                )
                            })}
                        </ImageContainer>
                    </HeroHalf>
                </HeroDiv>
            </HeroSection>
        </>
    )
}
Hero.propTypes = {
    images: PropTypes.array,
    title: PropTypes.string,
    button: PropTypes.string
}
export default Hero
