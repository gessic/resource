import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Post from '../Post';
import Slider from 'react-slick'
import './postgroup.css'
import MoreStories from "../MoreStories";

const PostGroupWrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: stretch;
  margin: 2em 2em;
  @media (max-width: 1000px) {
    flex-direction: column;
    margin: 2em 1em;
  }
`;

const PostGroup = ({postsItem}) => {
    const settings = {
        dots: true,
        infinite: true,
        speed: 900,
        slidesToShow: 3,
        autoplay: true,
        variableWidth: true,
        slidesToScroll: 1,
        easing: 'linear',
        // className: 'slick-track',
        arrows: false,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    infinite: true,
                    dots: true
                }
            },
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    initialSlide: 2
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    }

    return (
        <>
            <MoreStories/>
            <PostGroupWrapper>
                <Slider {...settings}>
                    {postsItem.map(post => {
                        return <div key={post.sys.id}><Post key={post.sys.id} post={post}/></div>
                    })}
                </Slider>
            </PostGroupWrapper>
        </>
    );
};

PostGroup.propTypes = {
    postsItem: PropTypes.array.isRequired,
};

export default PostGroup;
