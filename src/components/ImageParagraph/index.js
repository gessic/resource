import React from "react";
import styled from "styled-components";
import {documentToReactComponents} from "@contentful/rich-text-react-renderer";
import {BLOCKS, MARKS} from "@contentful/rich-text-types";
import PropTypes from "prop-types";


const ImageParagraph = (props) => {
    const { image, storyText, textAlignTop} = props.fields
    //console.log(props.fields)
    const options = {
        renderMark: {
            [MARKS.BOLD]: text => <b>{text}</b>,
        },
        renderNode: {
            [BLOCKS.PARAGRAPH]: (node, children) => <ArticleItemText>{children}</ArticleItemText>,
            [BLOCKS.HEADING_2]: (node, children) => <ArticleItemTitle>{children}</ArticleItemTitle>,
            [BLOCKS.HEADING_3]: (node, children) => <ArticleItemBlockQuote>{children}</ArticleItemBlockQuote>,
            [BLOCKS.UL_LIST]: (node, children) => {
                return(
                    <UnOrderedList>{children}</UnOrderedList>
                )
            },
        },
    }
    return (
        <Container textAlignTop={textAlignTop}>
            <ImageContainer>
                <img src={image?.fields?.file?.url}
                alt={image?.fields?.title}/>
            </ImageContainer>
            <RichTextContainer>
                {documentToReactComponents(storyText, options)}
            </RichTextContainer>
        </Container>
    )
}

ImageParagraph.proptypes = {
    props: PropTypes.object.isRequired
}
const Container = styled.div`
  display: flex;
  flex-direction: row;
  margin: 30px 150px;
  align-items: ${props => props.textAlignTop ? 'top' :  'center'};
  @media(max-width: 768px){
    margin: 30px 10px;
  }
  @media(max-width: 576px){
    flex-direction: column;
  }
`
const ImageContainer = styled.div`
  margin-right: 30px;
  text-align: center;
  img{
    width: 250px;
  }
  @media(max-width: 576px){
    margin-right: 0;
  }
`
const RichTextContainer = styled.div`
  width: 50%;
  @media(max-width: 576px){  
    width: 100%;
  }
`
const ArticleItemTitle = styled.h1`
  font-weight: bold;
  font-family: "Merriweather Sans", sans-serif;
  font-size: 24px;
  @media (max-width: 768px) {
    margin: 30px 10px;
  }

`
const ArticleItemText = styled.p`
  font-family: "Merriweather", sans-serif;
  @media (max-width: 768px) {
  }
`
 const ArticleItemBlockQuote = styled.p`
  font-size: 28px;
  font-family: "Merriweather Sans", sans-serif;
  @media (max-width: 768px) {
  }
`
const UnOrderedList = styled.ul`
  margin-left: 30px;
  li {
    list-style-type: disc;
    p {
      margin: 0 !important;
    }
    @media (max-width: 768px) {
    }
  }
`

export default ImageParagraph
