import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Fade from 'react-reveal/Fade';

const StatsItemWrapper = styled.div`
  color: white;
`;

const CountWrapper = styled.div`
`;

//Component to Render Stat item passed as crop
const StatsItem = ({ stat }) => {

    return (
        <StatsItemWrapper className="grid-item--full hero">
            <Fade >
                <CountWrapper>
                    <div>{stat}</div>
                </CountWrapper>
            </Fade>

        </StatsItemWrapper>
    );
};

StatsItem.propTypes = {
    stat: PropTypes.string.isRequired,
    index: PropTypes.number.isRequired
};

export default StatsItem;
