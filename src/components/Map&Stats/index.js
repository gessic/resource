import React from "react"
import styled from "styled-components"
import "./styles.css"
import Map from '../Map'
import StatsItem from "./statsitem";
import ScrollAnimation from 'react-animate-on-scroll'
import PropTypes from "prop-types";

//Map and Stats Section on Homepage
export default function MapStats({year, stats}) {
    return (

        <HeroSection
            className=" columns  is-flex is-flex-direction-column  is-justify-content-center   is-align-items-center ">
            <HeroTitleSection
                className="is-flex  is-flex is-flex-direction-column is-justify-content-space-around   is-align-items-center">
                <HeroTitle> Backing the ecosystem <span style={{display: 'inline-block'}}>
                    leaders  who back  diverse founders</span>
                </HeroTitle>
                <ActionLink href="#about"> Learn More </ActionLink>
            </HeroTitleSection>
            <Map/>
            <ScrollAnimation animateOnce={true} animateIn="animate__fadeInUp">
                <YearContainer>
                    <YearTitle>{year}</YearTitle>
                </YearContainer>
            </ScrollAnimation>
            <ScrollAnimation animateOnce={true} animateIn="animate__fadeInUp" delay={500}>
                <Description one={true}>
                    <svg xmlns="http://www.w3.org/2000/svg" width="1000" height="391" viewBox="0 0 1160 391">
                        <g id="Group_49" data-name="Group 49" transform="translate(-731 -1290)">
                            <path id="Path_114"
                                  d="M268.4,6812s197.427,306,517.619,306c320.163,0,529.548-306,529.548-306"
                                  transform="translate(532.433 -5496.999)" fill="none" stroke="#fff" strokeWidth="3"
                                  strokeDasharray="3"/>
                            <g id="Group_46" data-name="Group 46" transform="translate(731 1290)">
                                <g id="Group_35" data-name="Group 35" transform="translate(34)">
                                    <g id="Ellipse_3" data-name="Ellipse 3" fill="#009db0" stroke="#fff"
                                       strokeWidth="5">
                                        <ellipse cx="35" cy="34.5" rx="35" ry="34.5" stroke="none"/>
                                        <ellipse cx="35" cy="34.5" rx="32.5" ry="32" fill="none"/>
                                    </g>
                                    <text id="_10M" data-name="$10M" transform="translate(8 43)" fill="#fff"
                                          fontSize="20" fontFamily="'Lato', sans-serif" fontWeight="700">
                                        <tspan x="0" y="0">$10M</tspan>
                                    </text>
                                </g>
                                <text id="_Capital_raised" data-name=" Capital raised" transform="translate(0 95)"
                                      fill="#515863" fontSize="20" fontFamily="'Lato', sans-serif"
                                      fontWeight="700">
                                    <tspan x="0" y="0" xmlSpace="preserve"> Capital raised</tspan>
                                </text>
                                <text id="by_cohort_I" data-name="by cohort I" transform="translate(18 120)"
                                      fill="#515863" fontSize="20" fontFamily="'Lato', sans-serif"
                                      fontWeight="700">
                                    <tspan x="0" y="0">by cohort I</tspan>
                                </text>
                                <text id="participant" transform="translate(18 147)" fill="#515863" fontSize="20"
                                      fontFamily="'Lato', sans-serif" fontWeight="700">
                                    <tspan x="0" y="0">participant</tspan>
                                </text>
                                <text id="ESOs" transform="translate(46 178)" fill="#515863" fontSize="20"
                                      fontFamily="'Lato', sans-serif" fontWeight="700">
                                    <tspan x="0" y="0">ESOs</tspan>
                                </text>
                            </g>
                            <g id="Group_41" data-name="Group 41" transform="translate(1145.755 1562)">
                                <g id="Group_33" data-name="Group 33" transform="translate(6)">
                                    <g id="Ellipse_3-2" data-name="Ellipse 3" fill="#009db0" stroke="#fff"
                                       strokeWidth="5">
                                        <ellipse cx="35" cy="34.5" rx="35" ry="34.5" stroke="none"/>
                                        <ellipse cx="35" cy="34.5" rx="32.5" ry="32" fill="none"/>
                                    </g>
                                    <text id="_89" data-name="89" transform="translate(20 46)" fill="#fff"
                                          fontSize="27" fontFamily="'Lato', sans-serif" fontWeight="700">
                                        <tspan x="0" y="0">89</tspan>
                                    </text>
                                </g>
                                <text id="Mentors" transform="translate(0 92)" fill="#515863" fontSize="20"
                                      fontFamily="'Lato', sans-serif" fontWeight="700">
                                    <tspan x="0" y="0">Mentors</tspan>
                                </text>
                            </g>
                            <g id="Group_45" data-name="Group 45" transform="translate(1384.2 1562)">
                                <g id="Group_32" data-name="Group 32" transform="translate(12)">
                                    <g id="Ellipse_3-3" data-name="Ellipse 3" fill="#009db0" stroke="#fff"
                                       strokeWidth="5">
                                        <ellipse cx="35" cy="34.5" rx="35" ry="34.5" stroke="none"/>
                                        <ellipse cx="35" cy="34.5" rx="32.5" ry="32" fill="none"/>
                                    </g>
                                    <text id="_7" data-name="7" transform="translate(27 46)" fill="#fff" fontSize="27"
                                          fontFamily="'Lato', sans-serif" fontWeight="700">
                                        <tspan x="0" y="0">7</tspan>
                                    </text>
                                </g>
                                <g id="Group_37" data-name="Group 37" transform="translate(0 69)">
                                    <text id="_Coalition_" data-name=" Coalition " transform="translate(0 22)"
                                          fill="#515863" fontSize="20" fontFamily="'Lato', sans-serif"
                                          fontWeight="700">
                                        <tspan x="0" y="0" xmlSpace="preserve"> Coalition</tspan>
                                    </text>
                                    <text id="_partners" data-name=" partners" transform="translate(5 45)"
                                          fill="#515863" fontSize="20" fontFamily="'Lato', sans-serif"
                                          fontWeight="700">
                                        <tspan x="0" y="0" xmlSpace="preserve"> partners</tspan>
                                    </text>
                                </g>
                            </g>
                            <g id="Group_42" data-name="Group 42" transform="translate(935.819 1468)">
                                <g id="Group_31" data-name="Group 31">
                                    <g id="Ellipse_3-4" data-name="Ellipse 3" fill="#009db0" stroke="#fff"
                                       strokeWidth="5">
                                        <ellipse cx="35" cy="34.5" rx="35" ry="34.5" stroke="none"/>
                                        <ellipse cx="35" cy="34.5" rx="32.5" ry="32" fill="none"/>
                                    </g>
                                    <text id="_23" data-name="23" transform="translate(20 46)" fill="#fff"
                                          fontSize="27" fontFamily="'Lato', sans-serif" fontWeight="700">
                                        <tspan x="0" y="0">23</tspan>
                                    </text>
                                </g>
                                <text id="ESOs-2" data-name="ESOs" transform="translate(11 97)" fill="#515863"
                                      fontSize="20" fontFamily="'Lato', sans-serif" fontWeight="700">
                                    <tspan x="0" y="0">ESOs</tspan>
                                </text>
                            </g>
                            <g id="Group_44" data-name="Group 44" transform="translate(1752 1290)">
                                <g id="Ellipse_3-5" data-name="Ellipse 3" transform="translate(37)" fill="#009db0"
                                   stroke="#fff" strokeWidth="5">
                                    <ellipse cx="35" cy="34.5" rx="35" ry="34.5" stroke="none"/>
                                    <ellipse cx="35" cy="34.5" rx="32.5" ry="32" fill="none"/>
                                </g>
                                <text id="_2.2K" data-name="2.2K" transform="translate(51 43)" fill="#fff"
                                      fontSize="21" fontFamily="'Lato', sans-serif" fontWeight="700">
                                    <tspan x="0" y="0">2.2K</tspan>
                                </text>
                                <g id="Group_39" data-name="Group 39" transform="translate(0 69)">
                                    <text id="Entrepreneurs" transform="translate(0 23)" fill="#515863" fontSize="21"
                                          fontFamily="'Lato', sans-serif" fontWeight="700">
                                        <tspan x="0" y="0">Entrepreneurs</tspan>
                                    </text>
                                    <text id="_supported_by" data-name=" supported by" transform="translate(4 50)"
                                          fill="#515863" fontSize="20" fontFamily="'Lato', sans-serif"
                                          fontWeight="700">
                                        <tspan x="0" y="0" xmlSpace="preserve"> supported by</tspan>
                                    </text>
                                    <text id="the_ESOs" data-name="the ESOs" transform="translate(29 77)" fill="#515863"
                                          fontSize="20" fontFamily="'Lato', sans-serif" fontWeight="700">
                                        <tspan x="0" y="0">the ESOs</tspan>
                                    </text>
                                </g>
                            </g>
                            <g id="Group_48" data-name="Group 48" transform="translate(1566 1468)">
                                <g id="Group_30" data-name="Group 30" transform="translate(72)">
                                    <g id="Ellipse_3-6" data-name="Ellipse 3" fill="#009db0" stroke="#fff"
                                       strokeWidth="5">
                                        <ellipse cx="35" cy="34.5" rx="35" ry="34.5" stroke="none"/>
                                        <ellipse cx="35" cy="34.5" rx="32.5" ry="32" fill="none"/>
                                    </g>
                                    <text id="_4.5" data-name="4.5" transform="translate(16 46)" fill="#fff"
                                          fontSize="27" fontFamily="'Lato', sans-serif" fontWeight="700">
                                        <tspan x="0" y="0">4.5</tspan>
                                    </text>
                                </g>
                                <g id="Group_47" data-name="Group 47" transform="translate(0 69)">
                                    <text id="Average_age_in_" data-name="Average age in " transform="translate(39 23)"
                                          fill="#515863" fontSize="21" fontFamily="'Lato', sans-serif"
                                          fontWeight="700">
                                        <tspan x="0" y="0">Average age in</tspan>
                                    </text>
                                    <text id="organizations" transform="translate(39 79)" fill="#515863" fontSize="21"
                                          fontFamily="'Lato', sans-serif" fontWeight="700">
                                        <tspan x="0" y="0">organizations</tspan>
                                    </text>
                                    <text id="years_of" data-name="years of" transform="translate(0 51)" fill="#515863"
                                          fontSize="21" fontFamily="'Lato', sans-serif" fontWeight="700">
                                        <tspan x="0" y="0">years of</tspan>
                                    </text>
                                    <text id="participating" transform="translate(84 51)" fill="#515863" fontSize="21"
                                          fontFamily="'Lato', sans-serif" fontWeight="700">
                                        <tspan x="0" y="0">participating</tspan>
                                    </text>
                                </g>
                            </g>
                        </g>
                    </svg>
                </Description>
            </ScrollAnimation>
            <ScrollAnimation animateOnce={true} animateIn="animate__fadeInUp">
                <Description two={true}>
                    {stats.map((stat, index) => {
                        return (
                            <DescriptionItem key={index}>
                                <Circle>
                                    {
                                        <StatsItem stat={`${stat?.fields?.stat}`}
                                                   index={index + 1}/>
                                    }

                                </Circle>
                                <DescriptionText>
                                    {stat?.fields?.statDescription}
                                </DescriptionText>
                            </DescriptionItem>
                        )
                    })}
                </Description>
            </ScrollAnimation>
        </HeroSection>
    )
}
MapStats.propTypes = {
    year: PropTypes.string.isRequired,
    stats: PropTypes.object.isRequired
}

const HeroSection = styled.section`
  background-size: cover;
  height: 100%;
  background-color: #E3EFF1;
  padding: 0 50px 50px;
  width: 100%;
  background-image: linear-gradient(to bottom, white, white 35%, #e3f1f3 35%);
  margin: 0 !important;
  @media (max-width: 576px) {
    background-image: linear-gradient(to bottom, white, white 30%, #e3f1f3 30%);
  ;
    padding: 30px 5px;
  }

  svg g {
    width: 100%;
  }

  @media (min-width: 577px) and (max-width: 768px) {

  }

`

const HeroTitle = styled.h1`
  font-weight: bold;
  color: #3C3C3C;
  font-size: 48px;
  text-align: center;
  padding-bottom: 20px;
  max-width: 900px;
  @media (max-width: 768px) {
    font-size: 32px;
    line-height: 33px;
    margin-top: 2em;
  }
`
const HeroTitleSection = styled.div`
  display: none !important;
  position: absolute;
  pointer-events: none;
  height: 4em;
  margin-top: 200px;
  width: auto;
  top: 0;
  left: 0;
  right: 0;
  @media (max-width: 768px) {
    margin-top: 120px;
  }
`
const ActionLink = styled.a`
  padding: 10px 40px;
  text-decoration: none;
  pointer-events: auto;
  border-radius: 15px;
  font-weight: 300;
  border: 3px solid #4c9caf;
  background-color: #E3EFF1;
  margin-top: 20px;
  color: #3C3C3C;

  &:hover {
    background-color: #4c9caf;
    color: white;
  }
`

const Description = styled.div`
  flex-direction: row;
  align-items: start;
  justify-content: center;
  width: 100%;
  ${props => props.one ? ' display: flex; ' : ' display: none'};
  @media (max-width: 1003px) {
    width: 100%;
    padding-bottom: 3rem;
    ${props => props.two ? ' display:flex;' : ' display: none;'};
    flex-direction: row;
    flex-wrap: wrap;
    align-items: center;
    justify-content: center;

  }
  @media (min-width: 577px) and (max-width: 768px) {
    width: 100%;
  }
  @media (min-width: 769px) and (max-width: 992px) {
    width: 100%;
  }
  @media (min-width: 769px) and (max-width: 1280px) {
    ${props => props.one ? 'margin-top: -10em;' : ''};
  }
  @media (min-width: 1281px) {
    ${props => props.one ? 'margin-top: -10em;' : ''};
  }
`
const DescriptionItem = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: start;
  width: 50%;
  height: 180px;
  margin-top: 1em;
  @media (max-width: 576px) {
    margin: 1em 0;
  }
`
const Circle = styled.div`
  width: 5rem;
  height: 5rem;
  border-radius: 50%;
  background-color: #3592A5;
  border: 4px solid white;
  display: flex;
  align-items: center;
  justify-content: center;
  color: white;
  font-weight: bold;
  @media (max-width: 576px) {
    width: 3.5rem;
    height: 3.5rem;

  }
`
const DescriptionText = styled.div`
  max-width: 10rem;
  font-weight: bold;
  color: #515863;
  text-align: center;
`
const YearContainer = styled.div`
  width: 100%;
`
const YearTitle = styled.h2`
  text-align: center;
  font-weight: bold;
  font-size: 42px;
  color: #3592A5;
`

