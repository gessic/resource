import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const TitleWrapper = styled.div`
  padding-top: 5em;

  h1 {
    text-align: center;
    font-size: 42px;
    font-weight: bold;
    padding-bottom: 1em;
  }

  h2 {
    text-align: center;
  }
`;

//Component with appropriate Styles for a title used across different Components
const Title = (props) => {
    const {title, subtitle, id, backgroundColor, color} = props;
    return (
        <TitleWrapper style={{backgroundColor: backgroundColor || 'white', color: color || 'black'}} id={id}>
            <h1>{title}</h1>
            {subtitle && <h2 className="subtitle">{subtitle}</h2>}
        </TitleWrapper>
    );
};

Title.propTypes = {
    props: PropTypes.object.isRequired,
};

export default Title;
