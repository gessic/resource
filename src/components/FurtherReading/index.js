import React from "react"
import styled from "styled-components"

export default function FurtherReading() {
    return (

        <div className="section">
            <TextBox>
                <h1 className="title">Apply to <span style={{fontFamily: "Helvetica", fontSize: 32}}> r<span
                    style={{color: "#4c9caf", fontWeight: "bold", fontFamily: "Arial"}}>eso</span>urce</span></h1>
                <h2 style={{marginTop: 20}} className="subtitle is-5"><b>Application Deadline:</b> August 27</h2>
                <p>Village Capital and The Black Innovation Alliance are accepting applications for the second iteration
                    of R<span style={{color: "#4c9caf", fontWeight: "bold", fontFamily: "Arial"}}>eso</span>urce, a
                    nationwide program to support and uplift entrepreneur support organizations (think incubators and
                    accelerators) that are led by and focused on founders of color.</p>
                <p>The goal of R<span style={{color: "#4c9caf", fontWeight: "bold", fontFamily: "Arial"}}>eso</span>urce
                    is to create a stronger entrepreneurial infrastructure for Black, Latinx, and Indigenous founders
                    across the United States, by running ESO accelerators and building a community of practice around
                    these ecosystem leaders. This will happen in two ways:</p>
                <br/>
                <ul>
                    <li><b>Trainings:</b> R<span
                        style={{color: "#4c9caf", fontWeight: "bold", fontFamily: "Arial"}}>eso</span>urce's "ESO
                        Accelerator” will train startup ecosystem leaders of color on how to build a more financially
                        sustainable organization, as well as curate connections to potential funders. Resource will
                        provide milestone-based financial support tied to organizational development.
                    </li>
                    <br/>
                    <li><b>Community:</b> R<span
                        style={{color: "#4c9caf", fontWeight: "bold", fontFamily: "Arial"}}>eso</span>urce will build a
                        national community of practice among ESO leaders of color and their funders – to share best
                        practices in entrepreneur support and develop stronger capital and mentorship pathways for
                        founders in communities across the country.
                    </li>
                </ul>
                <p>For more information, reach out to Dahlia Joseph of Village Capital (<a
                    href="mailto:dahlia.joseph@vilcap.com">dahlia.joseph@vilcap.com)</a> or Bianca St. Louis of Black
                    Innovation Alliance <a
                        href="bstlouis@blackinnovationalliance.com">(bstlouis@blackinnovationalliance.com)</a></p>
                <button onClick={() => window.location = "https://eso.abaca.app/entrepreneurs/program/?a=resource2"}
                        style={{backgroundColor: '#4c9caf', fontWeight: 'bold', marginTop: 40, padding: '25px 40px'}}
                        className="button is-info is-flex is-align-self-flex-end"> Apply
                </button>
            </TextBox>
        </div>
    )
}

const TextBox = styled.div`
  padding: 50px 50px 100px 50px;
  border-radius: 4px;
  font-size: 20px;
  box-shadow: 5px 5px 15px #ddd, -5px -5px 15px #ddd;
  background: white;
  max-width: 900px;
  margin: auto;
  display: flex;
  flex-direction: column;
  @media (max-width: 768px) {
    font-size: 12px;
  }

  p {
    margin-top: 20px;
  }

  li {
    margin-top: 5px;
  }

  ul {
    margin-top: 40px;
  }
`;
