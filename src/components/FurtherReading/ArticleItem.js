import React from "react"

import {ArticleCard} from '../'


export default function ArticleItem(props) {


    return (
        <div className="column">
            <ArticleCard class="card" style={{maxWidth: 340}}>
                <div className="card-image">
                    <figure className=" ">
                        <a href={props.article.articleLink}>
                            <img src={props.article.image} alt={props.article.title}/>
                        </a>
                    </figure>
                </div>
                <div className="card-content">

                    <div className="media-content">
                        <a href={props.article.articleLink}>
                            <p style={{paddingBottom: 10}} className="title is-4">
                                {props.article.title}
                            </p>
                        </a>
                        <p style={{paddingBottom: 10}} className="subtitle is-5"> {props.article.subTitle} </p>
                    </div>
                    <div className="content">
                        <p> {props.article.description} </p>
                    </div>
                </div>
            </ArticleCard>
        </div>
    )
}
