import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
//import RichTextRenderer from "../../renderer/RichTextRenderer";

const CardWrapper = styled.div`
  display: flex;
  align-items: stretch;
  margin: 1em 2em 2em;
  background-color: #F9F9F9;
  max-width: 300px;
  min-height: 420px;
  @media (min-width: 1024px) and (max-width: 1200px) {
    margin: 1em .5em 2em;
  }
`;

const TextBox = styled.div`
  display: flex;
  flex-direction: column;
  max-width: 300px;

  a {
    text-decoration: none;
    color: #0094a8;
  }

  img {
    width: 300px;
    height: 200px;
    object-fit: cover;
  }

  p {
    padding: 0.5em 1em;
  }

  h1 {
    padding: 0.5em 0.75em;
    font-size: 18px;
    font-weight: bold;
  }

  @media (max-width: 1500px) {
    max-width: 300px;
    img {
      width: 300px;
      height: 225px;
      object-fit: cover;
    }
  }
  @media (max-width: 1000px) {
    margin: auto;
    max-width: 375px;
    padding-bottom: 2em;
    img {
      width: 375px;
      height: 275px;
      object-fit: cover;
      border-radius: 5px;
      margin: auto;
    }
  }

`;

const Text = styled.p`
  color: #0094a8;
  font-weight: bold;
`;

const Card = ({post}) => {
    const {caption, thumbnail, content, url} = post.fields;
    const {title, subtitle} = content.fields;
    return (
        <CardWrapper>
            <TextBox>
                <a href={url} target="_blank" rel="noopener noreferrer">
                    <img src={thumbnail.fields.file.url}
                         alt={thumbnail.fields.title}/>
                </a>
                <Text> {caption && `${caption}  `} </Text>
                <a href={url} target="_blank" rel="noopener noreferrer">
                    <h1>{title}</h1>
                </a>
                <p>{subtitle}</p>
            </TextBox>
        </CardWrapper>
    );
};

Card.propTypes = {
    post: PropTypes.object.isRequired,
};

export default Card;
