import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const ParallaxWrapper = styled.div`
    background-image: url(${props => props.image});
    min-height: 250px; 
    background-attachment: fixed;
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover; 
`;

const Parallax = ({parallaxImage}) => {
  const { file, title } = parallaxImage.fields;

  return (
    <div className=" is-hidden-mobile ">
        <ParallaxWrapper image={`https:${file.url}`} />
  <p style={{float: 'right',  paddingRight: 50}}>Source: {title}</p>
    </div>
  );
};


Parallax.propTypes = {
    parallaxImage: PropTypes.object.isRequired,
  };

export default Parallax;
