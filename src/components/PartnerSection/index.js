import React, {Fragment} from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const PartnerSectionWrapper = styled.div`
  background-color: white;
  display: flex;
  justify-content: center;
  padding: 2em 4em;
  margin: 0;

  img {
    max-height: 3em;
  }

  @media (max-width: 768px) {
    padding: 2em 2em;
  }
`;

const PartnerItem = styled.li`
  padding: 1.5em 0;
  @media (max-width: 620px) {
    margin: 0 4em;
  }
`;

const PartnerGroup = styled.ul`
  display: flex;
  flex-wrap: wrap;
  justify-content: space-around;
  align-items: center;

  .break {
    flex-basis: 100%;
    height: 0;
  }

  @media (max-width: 610px) {
    display: flex;
    justify-content: center;
    align-items: center;
  }

`;

const PartnerSection = ({partnerItem}) => {

    return (
        <PartnerSectionWrapper className="is-flex">
            <PartnerGroup>
                {partnerItem.map((partner, index) => {
                    return (
                        index === 1 ?
                            <Fragment key={partner.sys.id}>
                                <div className="break"/>
                                <PartnerItem key={partner.sys.id}>
                                    <img
                                        src={`https:${partner?.fields?.logo?.fields?.file?.url}`}
                                        alt={`${partner?.fields?.logo?.fields?.description}`}/>
                                </PartnerItem>
                            </Fragment>
                            :
                            (index) % 4 === 0 ?
                                <Fragment key={partner.sys.id}>
                                    <div className="break"/>
                                    <PartnerItem key={partner.sys.id}>
                                        <img
                                            src={`https:${partner?.fields?.logo?.fields?.file?.url}`}
                                            alt={`${partner?.fields?.logo?.fields?.description}`}/>
                                    </PartnerItem>
                                </Fragment>
                                :
                                <PartnerItem key={partner.sys.id}>
                                    <img
                                        src={`https:${partner?.fields?.logo?.fields?.file?.url}`}
                                        alt={`${partner?.fields?.logo?.fields?.description}`}/>
                                </PartnerItem>
                    )
                })}
            </PartnerGroup>
        </PartnerSectionWrapper>
    );
};

PartnerSection.propTypes = {
    partnerItem: PropTypes.object.isRequired
};

export default PartnerSection;
