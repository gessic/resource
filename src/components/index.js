//import  React from "react"
import styled from 'styled-components';
import GroupPhoto from '../images/groupPhoto.jpg'

export const Container = styled.div`
  
`;

export const FooterSection = styled.section`
  border-top-width: 1px;
  border-color: black;
  background-color: #DDD;
  padding-right: 100px;
`;


export const Parallax = styled.div`
background-image: url(${GroupPhoto});
min-height: 250px; 
background-attachment: fixed;
background-position: center;
background-repeat: no-repeat;
background-size: cover; 
`

export const HighlightedLabel = styled.div`
  background-color: green;
`


export const ArticleCard = styled.div`
  max-width: 340px;
`

