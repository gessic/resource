import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import AccordionItem from './AccordionItem';

const AccordionWrapper = styled.div`
  margin: 100px auto;
  max-width: 1024px;
  @media (max-width: 768px) {
    margin-left: 0;
    margin-right: 0;
  }
`;

//Accordion Component
const Accordion = ({accordionItem}) => {
  return (
   <AccordionWrapper>
       {accordionItem.map(item => {
           return <AccordionItem key={item.sys.id} content={item} />
       })}
   </AccordionWrapper>
  );
};

Accordion.propTypes = {
    accordionItem: PropTypes.array.isRequired,
  };

export default Accordion;
