import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import RichTextRenderer from "../../renderer/RichTextRenderer";
import AngleDownIcon from '../../images/icons/angle-down-light.svg'
import MinusIcon from '../../images/icons/minus.svg'

const AccordionItemWrapper = styled.div`
    margin: auto;
    background-color: #E3EFF1;
    max-width: 1024px; 
    @media (max-width: 768px) {
        margin-left: 0;
        margin-right: 0;
    }
`;

const AccordionHeaderRow = styled.div`  
    padding: 20px 40px;
    border-bottom: 1px solid #fff;
    .icon{
        position: relative;
        top:15px;
    }
`

const AccordionHeaderRowH1 = styled.h1 `
    color:#3d424c;
    font-weight: bold;
    font-size: 22px;
    display:flex;
    align-items: center;
`

const AccordionSection = styled.section `
    background-color: #fff;
    padding-left: 2em;
    padding-top: 2em;
    .accordion-header{
        font-weight: 600;
        font-size: 20px
    }
    p {
        margin-top: 2em;
        margin-bottom: 2em;
        margin-left: 1em;
    }
    h3 {
        font-size: 20px;
        font-weight: bold;
    }
`
const CollapsingDiv = styled.div`
  cursor: pointer;
`

const AccordionItem = ({content}) => {
    const [ open, setOpen ] = React.useState(false)
    const {title, accordionContent} = content.fields;

  return (
   <AccordionItemWrapper>
        <CollapsingDiv  onClick={() => {setOpen(!open)}}>
            <AccordionHeaderRow  className="is-flex is-justify-content-space-between"  >
                <AccordionHeaderRowH1>  {title} </AccordionHeaderRowH1>
                {open ? <img  alt='' src={MinusIcon} className='icon' />  : <img  alt='' src={AngleDownIcon} className='icon' /> }
            </AccordionHeaderRow>
        </CollapsingDiv>
            {open &&
            <AccordionSection >
              {RichTextRenderer.render(accordionContent)}
            </AccordionSection>
             }
   </AccordionItemWrapper>
  );
};

AccordionItem.propTypes = {
    content: PropTypes.object.isRequired,
  };

export default AccordionItem;
