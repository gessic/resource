import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
// import LinkedInLogo from '../../images/icons/linkedinIcon.svg';

const LinkedInLogo = (props) => (
    <svg fill={props.fill} className={props.class} xmlns="http://www.w3.org/2000/svg" width="20" height="20"
         viewBox="0 0 38 38">
        <g id="Icon_" data-name="Icon " transform="translate(0.094 0.094)">
            <rect id="Area_ICON:feather_linkedin_SIZE:MEDIUM_STYLE:STYLE2_"
                  data-name="Area [ICON:feather/linkedin][SIZE:MEDIUM][STYLE:STYLE2]" width="38" height="38"
                  transform="translate(-0.094 -0.094)" fill="rgba(253,73,198,0.35)" opacity="0"/>
            <g id="Icon" transform="translate(3.159 3.159)">
                <path id="Path"
                      d="M17.81,6.667a9.475,9.475,0,0,1,9.477,9.477V27.2H20.969V16.144a3.159,3.159,0,1,0-6.317,0V27.2H8.333V16.144A9.476,9.476,0,0,1,17.81,6.667Z"
                      transform="translate(4.302 2.81)" stroke={props.stroke} strokeLinecap="round"
                      strokeLinejoin="round" strokeWidth="2"/>
                <rect id="Rect" width="6.318" height="18.953" transform="translate(0 11.056)" stroke={props.stroke}
                      strokeLinecap="round" strokeLinejoin="round" strokeWidth="2"/>
                <circle id="Path-2" data-name="Path" cx="3.159" cy="3.159" r="3.159" transform="translate(0 0)"
                        stroke={props.stroke} strokeLinecap="round" strokeLinejoin="round" strokeWidth="2"/>
            </g>
        </g>
    </svg>
)

const TwitterLogo = (props) => (
    <svg fill={props.fill} className={props.class} xmlns="http://www.w3.org/2000/svg" width="20" height="20"
         viewBox="0 0 38.209 37">
        <g id="Icon_" data-name="Icon " transform="translate(-0.057 -0.122)">
            <rect id="Area_ICON:feather_twitter_SIZE:MEDIUM_STYLE:STYLE2_"
                  data-name="Area [ICON:feather/twitter][SIZE:MEDIUM][STYLE:STYLE2]" width="38" height="37"
                  transform="translate(0.266 0.122)" fill="rgba(253,73,198,0.35)" opacity="0"/>
            <g id="Icon" transform="translate(0.891 2.7)">
                <path id="Path"
                      d="M37.3,2.51a17.662,17.662,0,0,1-5.206,2.7,7.133,7.133,0,0,0-8.251-2.2A7.91,7.91,0,0,0,19.067,10.5V12.26A17.353,17.353,0,0,1,4.149,4.272s-6.63,15.87,8.289,22.925a18.455,18.455,0,0,1-11.6,3.525c14.918,8.817,33.153,0,33.153-20.277a8.4,8.4,0,0,0-.133-1.464A13.839,13.839,0,0,0,37.3,2.51Z"
                      transform="translate(-0.833 -2.491)" stroke={props.stroke} strokeLinecap="round"
                      strokeLinejoin="round" strokeWidth="2.5"/>
            </g>
        </g>
    </svg>
)

const PersonWrapper = styled.div`
  background-color: ${props => props.backgroundColor};
  display: flex;
  flex: 1;
  text-align: center;
  flex-direction: column;
  align-items: center;
  color: white;
  position: relative;

  h3 {
    font-size: 20px;
    font-weight: bold;
  }



`;

const Wrapper = styled.div`
  background-color: ${props => props.backgroundColor};
  margin: 2em ${props => props.auto};
  max-width: 17em;
  min-height: ${props => props.minHeight};
  display: flex;
  flex: 1;
  @media (max-width: 576px) {
    margin: 1.5em auto;
  }
  @media (max-width: 386px) {
    margin: 2em auto;
  }
`;

const Image = styled.img`
  max-width: 17em;
  object-fit: contain;
`;
const Logos = styled.div`
  text-align: center;
  display: flex;
  flex: 1;
  justify-content: center;
  align-items: center;
  margin: 1em 0;
`;
const DetailWrapper = styled.div`
  width: 100%;
  text-align: start;

  h3 {
    font-size: 1.5rem;
  }

`

const Person = ({person, carousel, minHeight, auto}) => {
    const [iconColor] = React.useState('white');
    const [strokeColor] = React.useState('white');
    const [backgroundColor] = React.useState('#3592A5');

    const {
        photo,
        name,
        organization,
        twitter,
        linkedIn,
        website,
        hasProfile,
        profileUrl
    } = person.fields;

    return (
        <Wrapper minHeight={minHeight} backgroundColor={backgroundColor} auto={auto} onMouseEnter={() => {
        }} onMouseLeave={() => {
        }}>
            <PersonWrapper backgroundColor={backgroundColor} carousel={carousel}>
                {hasProfile ?
                    <a href={profileUrl}>
                        <Image
                            src={`https:${photo && photo?.fields?.file?.url}`}
                            alt={photo && photo?.fields?.description}
                        />
                    </a> :
                    <Image
                        src={`https:${photo && photo?.fields?.file?.url}`}
                        alt={photo && photo?.fields?.description}
                    />}
                <DetailWrapper>
                    <h3 style={{minHeight: 70, textAlign: 'center'}}>{name}</h3>
                    <a style={{color: "inherit"}} href={website} target="_blank" rel="noopener noreferrer">
                        <h4 style={{
                            minHeight: 20,
                            textAlign: 'center'
                        }}> {organization && organization?.fields?.name}</h4>
                    </a>

                </DetailWrapper>

                <Logos carousel={carousel}>

                    <a style={{paddingTop: 20, paddingLeft: 10, paddingRight: 10}} href={linkedIn} target="_blank"
                       rel="noopener noreferrer">
                        <LinkedInLogo stroke={strokeColor}
                                      class="logo"
                                      fill={iconColor}/>
                    </a>
                    <a style={{paddingTop: 20, paddingLeft: 10, paddingRight: 10}} href={twitter} target="_blank"
                       rel="noopener noreferrer">
                        <TwitterLogo stroke={strokeColor}
                                     class="logo"
                                     fill={iconColor}/>
                    </a>
                </Logos>
            </PersonWrapper>
        </Wrapper>
    );
};

Person.propTypes = {
    person: PropTypes.object.isRequired,
};

export default Person;
