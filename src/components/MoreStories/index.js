import React from "react";
import styled from "styled-components";

const MoreStoriesWrapper = styled.div`
  background-color: white;
  margin: 2em 0;
  padding: 2em 0;

  h1 {
    text-align: center;
    font-size: 42px;
    font-weight: bold;
  }

  h2 {
    text-align: center;
  }
`;
//const color = '#BD5E8C'

const LinkWrapper = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  color: #BD5E8C;

  a {
    margin: 0 .5em;
    color: #BD5E8C;
  }

  svg {
    margin-right: .5em;
  }
  h1 {
    color: #BD5E8C;
  }
`;
const Title = styled.h1`
  color: #BD5E8C;
  @media(max-width: 768px){
    width: 5em;
  }
`
const Line = styled.div`
  display: block;
  border-bottom: 1px solid rgba(0, 0, 0, 0.2);
  width: 10rem;
  @media(max-width: 576px){
    width: 5rem;
  }
`
const LinKIconWrapper = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  margin: 0 5em;
  @media(max-width: 576px){
    margin: 0 .5em;
  }
  @media(max-width: 992px) and (min-width: 577px){
    margin: 0 2em;
  }
`


const MoreStories = () => {
    const title = 'Read All Profiles'
    return (
        <MoreStoriesWrapper>
            <LinkWrapper>
                <Line/>
                <LinKIconWrapper>
                    <a href="/profiles" ><Title>{title}</Title></a>
                </LinKIconWrapper>
                <Line/>
            </LinkWrapper>
        </MoreStoriesWrapper>
    )

}

export default MoreStories;
