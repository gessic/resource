import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Person from '../Person';
import Slider from "react-slick";

const PersonGroupWrapper = styled.div`
  display: ${props => props.carousel ? 'block' : 'flex'};
  flex-wrap: wrap;
  flex-direction: row;
  justify-content: center;
  align-items: stretch;
  margin: 2em 5em;
  padding-top: 2em;
  .slick-track {
    padding-left: 1em;
  }
  @media (max-width: 576px) {
    margin: 2em 0;
    height: 100%;
    .slick-track {
        padding-left: 0;
      }
  }
`;

const PersonGroup = ({persons, carousel}) => {
    const settings = {
        dots: true,
        infinite: true,
        speed: 900,
        slidesToShow: 5,
        autoplay: true,
        variableWidth: false,
        slidesToScroll: 1,
        easing: 'linear',
        className: 'slick-track',
        responsive: [
            {
                breakpoint: 1600,
                settings: {
                    slidesToShow: 4,
                    infinite: true,
                    dots: true
                }
            },
            {
                breakpoint: 1286,
                settings: {
                    slidesToShow: 3,
                    infinite: true,
                    dots: true,
                    centerMode: false
                }
            },
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    initialSlide: 2,
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                }
            },
            {
                breakpoint: 575,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows: false,
                }
            },
        ]
    }
    return (
        <PersonGroupWrapper carousel={carousel}>
            {carousel ?
                <Slider {...settings}>
                    {persons.map(person => {
                        return <div key={person.sys.id}><Person auto={"auto"} key={person?.sys?.id} person={person} carousel={carousel}/></div>
                    })}
                </Slider>
                :
                persons.map(person => {
                    return <div key={person?.sys?.id}>
                        <Person key={person?.sys?.id} auto={"2em"} person={person} minHeight={'30em'} carousel={carousel}/>
                    </div>
                })
            }

        </PersonGroupWrapper>
    );
};

PersonGroup.propTypes = {
    persons: PropTypes.array.isRequired,
    carousel: PropTypes.bool.isRequired
};

export default PersonGroup;
