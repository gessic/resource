import React from "react"
import { Link } from "gatsby"

export default function Logo() {
    return (
        <Link to="/">
            <h3 style={{ display: `inline` }}>rESOurce</h3>
        </Link>
    )
}
