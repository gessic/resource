import React, {useState, useEffect} from "react"
import {Link} from "gatsby"
import styled from 'styled-components'
import Scrollspy from 'react-scrollspy'
import logoSrc from "../../images/resource-header-logo.svg"


export default function Header(props) {
    const [isActive, setisActive] = React.useState(false)
    const [scroll, setScroll] = useState(true)

    useEffect(() => {
        const onScroll = () => {
            const scrollCheck = window.scrollY < 100
            if (scrollCheck !== scroll) {
                setScroll(scrollCheck)
            }
        }
        document.addEventListener("scroll", onScroll)
        return () => {
            document.removeEventListener("scroll", onScroll)
        }
    }, [scroll, setScroll])
    const hideNav = () => {
        setisActive(false)
    }

    return (
        <Nav
            className={`navbar is-transparent is-fixed-top ${scroll ? "" : "nav-small"}`}
            role="navigation" aria-label="main navigation"
        >
            <div className="navbar-brand">
                {scroll && props.index ?
                    <Link to='/'>
                        <Brand>
                            resource
                            <BorderWrap>
                                <BorderSm/>
                                <Border/>
                            </BorderWrap>
                        </Brand>
                    </Link>
                    :
                    <Link to="/">
                        <LogoImg src={logoSrc} alt="logo"/>
                    </Link>
                }
                <a
                    onClick={() => {
                        setisActive(!isActive)
                        console.log('isActive: ', isActive)
                    }}
                    role="button"
                    className={`navbar-burger burger ${isActive ? 'is-active' : ''}`}
                    aria-label="menu"
                    aria-expanded="false"
                    data-target="navbarBasicExample"
                >
                    <span aria-hidden="true"/>
                    <span aria-hidden="true"/>
                    <span aria-hidden="true"/>
                </a>
            </div>

            <div id="navbarBasicExample"
                 className={`navbar-menu ${isActive ? 'is-active' : ''}`}
            >
                <Scrollspy items={['about', 'companySection', 'esos', 'getInvolved']} currentClassName="is-current"
                           componentTag={'div'} className="navbar-end">
                    <ScrollspyItem href="/#about" className="navbar-item">
                        <LinkSpan
                            scroll={scroll} index={props?.index} onClick={hideNav}>About</LinkSpan>
                    </ScrollspyItem>
                    <ScrollspyItem href="/profiles" className="navbar-item">
                        <LinkSpan scroll={scroll}
                                  index={props?.index}>Stories</LinkSpan>
                    </ScrollspyItem>
                    <ScrollspyItem href="/#partners" className="navbar-item">
                        <LinkSpan
                            scroll={scroll} index={props?.index} onClick={hideNav}>Partners</LinkSpan>
                    </ScrollspyItem>
                    <ScrollspyItem href="/community" className="navbar-item">
                        <LinkSpan
                            scroll={scroll} index={props?.index}>Community</LinkSpan>
                    </ScrollspyItem>
                    <ScrollspyItem href="/#cohort" className="navbar-item">
                        <LinkSpan scroll={scroll}
                                  index={props?.index}  onClick={hideNav}>Current
                            Cohort</LinkSpan>
                    </ScrollspyItem>
                    <ScrollspyItem href="/#faq" className="navbar-item">
                        <LinkSpan
                            scroll={scroll} index={props?.index}  onClick={hideNav}>FAQ</LinkSpan>
                    </ScrollspyItem>

                </Scrollspy>
            </div>
        </Nav>
    )
}

const Nav = styled.nav`
  background-color: transparent;
  transition: all 0.3s;

  img {
    transition: width 0.3s;
    width: 80%;
    margin-left: 30px;
  }

  &&.nav-small {
    background-color: #fff;
    box-shadow: rgba(99, 99, 99, 0.2) 0 2px 8px 0;
  }

  &&.nav-small img {
    width: 40%;
    margin-top: 10px;
  }

  .navbar-end {
    align-items: center;
    margin-right: 20px;
  }
`

const ScrollspyItem = styled.a`
  &&:hover {
    color: #4c9caf;
  }

  &&.is-current {
    color: #4c9caf;
    border-radius: 5px;
  }

  &&.is-current:hover {
    color: #4c9caf;
  }
`
const LogoImg = styled.img`
  margin-bottom: 0;
  padding-top: 10px;
  margin-left: 50px;
`;

const LinkSpan = styled.span`
  color: ${props => props.scroll && props.index ? 'white' : 'auto'};
  @media (max-width: 1023px) {
    color: unset;
  }

`
const Brand = styled.div`
  border-top: 5px solid white;
  color: white;
  margin-top: .5em;
  margin-left: 2em;
`
const BorderWrap = styled.div`
  display: flex;
  flex-direction: row;
  width: 100%;
  height: 7px;
`
const BorderSm = styled.div`
  border-top: 5px solid white;
  margin-right: 1em;
  width: 30%;
`
const Border = styled.div`
  border-top: 5px solid white;
  width: 50%;
`

