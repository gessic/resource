import React from "react"
import { Link } from "gatsby"


const ListLink = props => (
  <li style={{ display: `inline-block`, marginRight: `1rem` }}>
    <Link to={props.to}>{props.children}</Link>
  </li>
)

export default function Nav() {
    return (
        <ul style={{ listStyle: `none`, float: `right` }}>
        <ListLink to="/">About</ListLink>
        <ListLink to="/about/">Program</ListLink>
        <ListLink to="/contact/">Funders</ListLink>
        <ListLink to="/contact/">Blog</ListLink>
        <ListLink to="/contact/">Get Involved</ListLink>
      </ul>
    )
}
