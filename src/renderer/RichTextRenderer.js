import { documentToReactComponents } from '@contentful/rich-text-react-renderer';


//Renders Rich Text into Html
class RichTextRenderer {
  static render(doc, options) {
    return new RichTextRenderer(doc, options).render();
  }

  constructor(doc, options) {
    this.doc = doc;
    this.options = options || {};
  }

  render() {
    return documentToReactComponents(this.doc, this.options);
  }
}

export default RichTextRenderer;
