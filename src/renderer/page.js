import React from 'react';
import {BLOCKS} from '@contentful/rich-text-types';
import PartnerSection from '../components/PartnerSection';
import Card from '../components/Card';
import Parallax from '../components/Parallax';
import Title from '../components/Title';
import PostGroup from '../components/PostGroup';
import Banner from '../components/Banner';
import ProfileItem from '../components/ProfileItem';
import PersonGroup from '../components/PersonGroup';
import Info from '../components/Info';
import Accordion from '../components/Accordion';
import MapStats from "../components/Map&Stats";
import Video from "../components/Video";
import Hero from "../components/Hero";

const componentMap = {
  partnerSection: PartnerSection,
  card: Card,
  parallax: Parallax,
  title: Title,
  postGroup: PostGroup,
  banner: Banner,
  profile: ProfileItem,
  personGroup: PersonGroup,
  info: Info,
  accordion: Accordion,
  map: MapStats,
  video: Video,
  hero: Hero
};

//Renders Data from Contentful to individual components in a page
const rendererOptions = () => {
  return {
    renderNode: {
      [BLOCKS.PARAGRAPH]: () => {
        return null;
      },
      [BLOCKS.EMBEDDED_ENTRY]: node => {
        try {
          const contentType = node.data.target.sys.contentType.sys.id;
          const Component = componentMap[contentType];
          if (!Component) return;
          return (
              <Component
                  id={node.data.target.sys.id}
                  {...node.data.target.fields}
                  key={node.data.target.sys.id}
              />
          );
        } catch (e) {
          return null;
        }
      }
    }
  };
};

export default rendererOptions;
