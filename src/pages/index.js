import React, {useState, useEffect} from "react"
import Seo from "../components/Seo/Seo";
import {Container} from '../components'
import Header from '../components/Header'
import Hero from '../components/Hero'
import Footer from '../components/Footer'
import {getPage} from '../scripts/getEntries';
import rendererOptions from "../renderer/page"
import RichTextRenderer from "../renderer/RichTextRenderer"
import "animate.css/animate.min.css";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import "../styles/styles.scss"
import "../styles/global.css"


// App Main Entry Point and also the Homepage
const IndexPage = () => {
    const [entry, setEntry] = useState({})
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        getPage('/')
            .then(entry => {
                setEntry(entry)
                typeof entry === 'undefined' ? setLoading(true) : setLoading(false);
            })
    }, [])

    if (loading)
        return (
            <>
                <Seo title='Resource'/>
            </>
        );


    const {pageContent, hero} = entry;
    const {content} = pageContent;

    return (
        <>
            <Seo title='Resource'/>
            {/*<Helmet>
        <meta charSet="utf-8" />
        <link rel="icon" href={favicon} />
        <title>Resource</title>
        <meta property="og:title" content="Resource" />
        <meta property="og:description" content="A country-wide project in the US to support entrepreneur support organizations led by and focused on founders of color." />
        <meta property="og:image" content={preview} />
        <meta property="twitter:card" content="summary_large_image" />
        <meta property="twitter:description" content="A country-wide project in the US to support entrepreneur support organizations led by and focused on founders of color." />
        <meta property="twitter:image" content={preview}/>
      </Helmet>**/}
            <Container>
                <Header index={true}/>
                <div id="home">
                    <Hero {...hero?.fields}/>
                </div>
                {content && content.map((item) => {
                    return RichTextRenderer.render(item, rendererOptions())
                })}
                <Footer/>
            </Container>
        </>
    )
}

export default IndexPage
