import React , { useEffect, useState } from "react"
import { Helmet } from "react-helmet"
import { Container } from '../components'
import Footer from '../components/Footer'
import favicon from '../images/favicon.svg'
import {getPage} from '../scripts/getEntries';
import rendererOptions from "../renderer/page"
import RichTextRenderer from "../renderer/RichTextRenderer"
import Header from "../components/Header"

//Community page
const Profiles = () => {
    const [entry, setEntry] = useState({})
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        getPage('/alumni')
            .then(entry => {
                setEntry(entry)
                typeof entry === 'undefined' ? setLoading(true) : setLoading(false);
            })
    }, [])

    if(loading)
        return (
            <Helmet>
                <meta charSet="utf-8" />
                <link rel="icon" href={favicon} />
                <title>Resource Alumni</title>
                <meta property="og:title" content="Resource" />
                <meta property="og:description" content="Meet the previous cohort of the resource program." />
                <meta property="og:image" content='https://images.ctfassets.net/59w37ga7xiqb/Ash3i08s0OkBoE9CGoQXk/071a56f85d5e8f8a24efd145c3f11387/twittercardimage.png' />
                <meta property="twitter:card" content="summary_large_image" />
                <meta property="twitter:description" content="Meet the previous cohort of the resource program." />
                <meta property="twitter:image" content='https://images.ctfassets.net/59w37ga7xiqb/Ash3i08s0OkBoE9CGoQXk/071a56f85d5e8f8a24efd145c3f11387/twittercardimage.png' />
            </Helmet>
        );

    const { pageContent } = entry;
    const { content } = pageContent;

    return (
        <>
            <Helmet>
                <meta charSet="utf-8" />
                <link rel="icon" href={favicon} />
                <title>Resource Alumni</title>
                <meta property="og:title" content="Resource" />
                <meta property="og:description" content="Meet the previous cohort of the resource program." />
                {/* <meta property="og:image" content={CohortList[6].image} /> */}
                <meta property="twitter:card" content="summary_large_image" />
                <meta property="twitter:description" content="Meet the previous cohort of the resource program." />
                {/* <meta property="twitter:image" content={CohortList[6].image} /> */}
            </Helmet>
            <Container>
                <Header />
                {content && content.map((item) => {
                    return RichTextRenderer.render(item, rendererOptions())
                })}
                <Footer />
            </Container>
        </>
    )
}

export default Profiles;
