import React, {useEffect, useLayoutEffect, useState} from "react"
import {Helmet} from "react-helmet"
import {Container} from '../components'
import Footer from '../components/Footer'
import favicon from '../images/favicon.svg'
import rendererOptions from "../renderer/page"
import RichTextRenderer from "../renderer/RichTextRenderer"
import Header from "../components/Header"
import styled from "styled-components";
import profileBanner from '../images/profileBanner.png'
import {getPage} from '../scripts/getEntries';

//Profiles Page
const Profiles = () => {
    const [entry, setEntry] = useState({})
    const [loading, setLoading] = useState(true);
    const [up, setUp] = useState(false)
    const [scrolled, setScrolled] = useState(false)
    const [once, useOnce] = useState(true)


    useEffect(() => {
        if (loading) {
            getPage('/profiles')
                .then(entry => {
                    setEntry(entry)
                    typeof entry === 'undefined' ? setLoading(true) : setLoading(false);
                })
        }

    })

    useEffect(() => {
        const onScroll = () => {
            let curtain = document.getElementById('curtain')
            let st = window.scrollY || document.documentElement.scrollTop
            if (st !== 0) {
                //scroll down
                if (!scrolled || !up) {
                   curtain.style.transform = 'translateY(-100vh)'
                    setUp(true)
                    setScrolled(true)
                    //console.log('scc', up)
                }
            } else {
                //scroll up
                if (st === 0 && curtain) {
                    curtain.style.transform = 'translateY(0vh)'
                    setUp(false)
                }
            }
        }
        document.addEventListener('scroll', onScroll)
        return () => {
            document.removeEventListener('scroll', onScroll)
        }
    }, [ scrolled, up])

    useLayoutEffect(() => {
        if (!loading && once) {
            let id = location.hash.split('#')[1]
            if (id) {
                const el = document.getElementById(id)
                setUp(true)
                setScrolled(true)
                startSlide()
                const top = window.scrollY + el.getBoundingClientRect().top - 68
                window.scrollTo({top, behavior: "auto"})
            }
            useOnce(false)
        }
    })

    if (loading)
        return (
            <Helmet>
                <meta charSet="utf-8"/>
                <link rel="icon" href={favicon}/>
                <title>Resource Profiles</title>
                <meta property="og:title" content="Resource"/>
                <meta property="og:description"
                      content="Learn more about the stories behind the entrepreneurs Resource supports"/>
                <meta property="og:image"
                      content="https://images.ctfassets.net/59w37ga7xiqb/1DPBq7p8b6PpeYlJ7JUHHa/21b22a2dcdf30a097954159119f15da9/illustrated_photos.png"/>
                <meta property="twitter:card" content="summary_large_image"/>
                <meta property="twitter:description"
                      content="Learn more about the stories behind the entrepreneurs Resource supports"/>
                <meta property="twitter:image"
                      content="https://images.ctfassets.net/59w37ga7xiqb/1DPBq7p8b6PpeYlJ7JUHHa/21b22a2dcdf30a097954159119f15da9/illustrated_photos.png"/>
            </Helmet>
        );

    const {pageContent} = entry ? entry : '';
    const {content} = pageContent ? pageContent : '';

    const startSlide = () => {
        let curtain = document.getElementById('curtain')
        curtain.style.transform = 'translateY(-100vh)'
        setUp(true)
    }

    return (
        <>
            <Helmet>
                <meta charSet="utf-8"/>
                <link rel="icon" href={favicon}/>
                <title>Resource Profiles</title>
                <meta property="og:title" content="Resource"/>
                <meta property="og:description"
                      content="Learn more about the stories behind the entrepreneurs Resource supports"/>
                <meta property="og:image"
                      content="https://images.ctfassets.net/59w37ga7xiqb/1DPBq7p8b6PpeYlJ7JUHHa/21b22a2dcdf30a097954159119f15da9/illustrated_photos.png"/>
                <meta property="twitter:card" content="summary_large_image"/>
                <meta property="twitter:description"
                      content="Learn more about the stories behind the entrepreneurs Resource supports"/>
                <meta property="twitter:image"
                      content="https://images.ctfassets.net/59w37ga7xiqb/1DPBq7p8b6PpeYlJ7JUHHa/21b22a2dcdf30a097954159119f15da9/illustrated_photos.png"/>
            </Helmet>
            <Container>
                <Curtain onClick={startSlide} id="curtain" image={profileBanner}>
                    <h1>
                        Stories
                    </h1>
                </Curtain>
                <Header/>
                <Contain>
                    <MonthYear>
                        <MonthText>
                            March 2022
                        </MonthText>
                    </MonthYear>
                    <CardContainer>

                        {content && content.map((item) => {
                            return RichTextRenderer.render(item, rendererOptions())
                        })}

                    </CardContainer>
                </Contain>

                <Footer/>
            </Container>
        </>
    )
}

export default Profiles;

const Contain = styled.div`
  margin-top: 6rem;
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  padding: 0 1em;

  @media (max-width: 576px) {
    padding: 0 .4em;
  }

`
const MonthYear = styled.div`
  padding-top: 4em;
  width: 8%;
`;
const CardContainer = styled.div`
  width: 90%;
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  align-items: flex-start;
  justify-content: space-evenly;
  @media (min-width: 1280px) {
    justify-content: start;
  }
`;
const MonthText = styled.div`
  color: #62A3B0;
  font-size: 68px;
  font-weight: bold;
  writing-mode: vertical-rl;
  text-orientation: sideways;
  font-family: poppins, sans-serif;
  transform: rotate(180deg);
  white-space: nowrap;
  @media (max-width: 576px) {
    font-size: 48px;
    margin-left: -.25em;
  }
`;
const Curtain = styled.div`
  top: 0;
  position: absolute;
  z-index: 100;
  background-image: linear-gradient(to bottom, rgba(27, 136, 153, 0.79) 100%, transparent 100%);
  width: 100%;
  height: 100vh;

  display: flex;
  flex-direction: column;
  justify-content: center;
  align-content: center;

  color: #FFFFFF;
  font-weight: 800;
  font-size: 5em;
  text-align: center;
  font-family: poppins, sans-serif;

  background-size: cover;
  background-position: 0 0;
  background-repeat: no-repeat;
  //opacity: 0.55;

  transition: all ease-out 1s;

  h1 {
    font-weight: bolder;
  }

`;

