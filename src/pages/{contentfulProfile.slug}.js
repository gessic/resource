import React  from "react"
import { Helmet } from "react-helmet"
import { Container } from '../components'
import Header from '../components/Header'
import Profile from "../components/Profile"
import Footer from '../components/Footer'
import favicon from '../images/favicon.svg'
import { graphql } from "gatsby";


const ProfilePage = (props) => {
    const profileData = props.data.contentfulProfile


    return (
        <>
            <Helmet>
                <meta charSet="utf-8" />
                <link rel="icon" href={favicon} />
                <title>Resource - {profileData.pageTitle}</title>
                <meta property="og:title" content={profileData.pageTitle} />
                <meta property="og:description" content={profileData.profileDescription} />
                <meta property="og:image" content={`http:${profileData.socialMediaPreviewImage.file.url}`} />
                <meta property="twitter:card" content="summary_large_image" />
                <meta property="twitter:description" content={profileData.profileDescription} />
                <meta property="twitter:image" content={`http:${profileData.socialMediaPreviewImage.file.url}`} />
                <meta name="keywords" content={profileData.keywords} />
            </Helmet>
            <Container>
                <Header />
                <Profile slug={props.uri} />
                <Footer />
            </Container>
        </>
    )
}

export default ProfilePage;

export const assetQuery = graphql`
query profileQuery($slug: String){
  contentfulProfile(slug: {eq: $slug}) {
        id
        slug
        profileName
        pageTitle
        profileCompanyLogo {
          file {
            url
          }
        }
        profilePhoto {
          file {
            url
          }
        }
        socialMediaPreviewImage {
          file {
            url
          }
        }
        profileDescription
        organizationName
    
  
  }
}
`
