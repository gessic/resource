import React  from "react"
import { Container } from '../components'
import Header from '../components/Header'
import FurtherReading from "../components/FurtherReading"
import Footer from '../components/Footer'
import Seo from "../components/Seo/Seo";


//How to apply page
const Apply = () => {

  return (
    <>
      <Seo title='Resource - Apply'/>
      <Container>
        <Header />
        <div style={{ marginTop: 150 }}>
        <FurtherReading />
        </div>
       <Footer/>
      </Container>
    </>
  )
}

export default Apply;















