module.exports = {
    "parser": "babel-eslint", // uses babel-eslint transforms
    "settings": {
        "react": {
            "version": "detect" // detect react version
        }
    },
    "env": {
        "browser": true,
        "es2021": true,
        "node": true
    },
    globals: {
        __PATH_PREFIX__: true,
    },
    "extends": [
        "eslint:recommended",
        "plugin:react/recommended",
        `react-app`,
    ],
    "parserOptions": {
        "ecmaFeatures": {
            "jsx": true
        },
        "ecmaVersion": "latest",
        "sourceType": "module"
    },
    "plugins": [
        "react"
    ],
    "rules": {
        "no-anonymous-exports-page-templates": "warn",
        "limited-exports-page-templates": "warn"
    }
}
