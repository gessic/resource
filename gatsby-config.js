require("dotenv").config({
    path: `.env`
})

const path = require("path");
// Get paths of Gatsby's required rules, which as of writing is located at:
// https://github.com/gatsbyjs/gatsby/tree/fbfe3f63dec23d279a27b54b4057dd611dce74bb/packages/
// gatsby/src/utils/eslint-rules
path.join(
    process.cwd(),
    "node_modules",
    "gatsby",
    "dist",
    "utils",
    "eslint-rules"
);
module.exports = {
    siteMetadata: {
        title: "Resource",
        description: 'A country-wide project in the US to support entrepreneur support organizations led by and focused on founders of color.',
        url: 'https://resource-initiative.com',
        image: 'https://images.ctfassets.net/59w37ga7xiqb/Ash3i08s0OkBoE9CGoQXk/071a56f85d5e8f8a24efd145c3f11387/twittercardimage.png',
    },
    flags: {},
    plugins: [
        "gatsby-plugin-react-helmet",

        "gatsby-plugin-image",
        "gatsby-plugin-sharp",
        "gatsby-transformer-sharp",
        {
            resolve: `gatsby-plugin-google-gtag`,
            options: {
                // You can add multiple tracking ids and a pageview event will be fired for all of them.
                trackingIds: [
                    "G-0HEBCZE82C", // Google Analytics / GA
                ],
                // This object gets passed directly to the gtag config command
                // This config will be shared across all trackingIds
                gtagConfig: {
                    optimize_id: "OPT_CONTAINER_ID",
                    anonymize_ip: true,
                    cookie_expires: 0,
                },
            },
        },
        `gatsby-plugin-sass`,
        {
            resolve: `gatsby-transformer-remark`,
            options: {
                // Footnotes mode (default: true)
                footnotes: true,
                // GitHub Flavored Markdown mode (default: true)
                gfm: true,
                // Plugins configs
                plugins: [],
            },
        },
        {
            resolve: `gatsby-plugin-typography`,
            options: {
                pathToConfigModule: `src/utils/typography`,
            },
        },
        {
            resolve: `gatsby-source-contentful`,
            options: {
                spaceId: process.env.GATSBY_spaceId,
                // Learn about environment variables: https://gatsby.dev/env-vars
                accessToken: process.env.GATSBY_accessToken,
                host: process.env.GATSBY_host
            },
        },
        {
            resolve: `gatsby-plugin-hotjar`,
            options: {
                includeInDevelopment: true, // optional parameter to include script in development
                id: 2333013,
                sv: 6,
            },
        },

        {
            resolve: "gatsby-source-filesystem",
            options: {
                name: "images",
                path: "./src/images/",
            },
            __key: "images",
        },
        "gatsby-plugin-styled-components",
        {
            resolve: `gatsby-plugin-page-creator`,
            options: {
                path: `${__dirname}/src/pages`,
                //ignore: [`foo-bar.js`],
                slugify: {
                    lowercase: false,
                    separator: '',
                }
            }
        }


    ],
};
