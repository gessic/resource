<p>
  <a href="https://www.gatsbyjs.com/?utm_source=starter&utm_medium=readme&utm_campaign=minimal-starter">
    <img alt="Gatsby" src="https://www.gatsbyjs.com/Gatsby-Monogram.svg" width="60" />
  </a>
</p>
<h1>
  Gatsby minimal starter
</h1>

## 🚀 Quick start

1.  **Create a Gatsby site.**

    Use the Gatsby CLI to create a new site, specifying the minimal starter.

    ```shell
    # create a new Gatsby site using the minimal starter
    npm init gatsby
    ```

2.  **Start developing.**

    Navigate into your new site’s directory and start it up.

    ```shell
    cd my-gatsby-site/
    npm run develop
    ```

3.  **Open the code and start customizing!**

    Your site is now running at http://localhost:8000!

    Edit `src/pages/profiles.js` to see your site update in real-time!

4.  **Learn more**

    - [Documentation](https://www.gatsbyjs.com/docs/?utm_source=starter&utm_medium=readme&utm_campaign=minimal-starter)

    - [Tutorials](https://www.gatsbyjs.com/tutorial/?utm_source=starter&utm_medium=readme&utm_campaign=minimal-starter)

    - [Guides](https://www.gatsbyjs.com/tutorial/?utm_source=starter&utm_medium=readme&utm_campaign=minimal-starter)

    - [API Reference](https://www.gatsbyjs.com/docs/api-reference/?utm_source=starter&utm_medium=readme&utm_campaign=minimal-starter)

    - [Plugin Library](https://www.gatsbyjs.com/plugins?utm_source=starter&utm_medium=readme&utm_campaign=minimal-starter)

    - [Cheat Sheet](https://www.gatsbyjs.com/docs/cheat-sheet/?utm_source=starter&utm_medium=readme&utm_campaign=minimal-starter)

# Resource Documentation


**Clone the repository and navigate to the directory**

```shell
  cd resource/
  ```
**Install Dependencies:**
```
  npm install
      or
  yarn install
```

**Run Project:**

```shell
  npm start
    or
  yarn start
```

The Project has 3 major pages:
    
   - HomePage
   - Profile Page
   - Community Page &
   - Individual Organization Pages

The Site content is stored on Contentful and one may need permissions to get access

The content is loaded from contentful using contentful sdk and custom scripts and rendered dynamically on individual 
components.

Components are found in the src/components/ folder.
The renderer is in the /src/render/ folder.

One also needs to be familiar with [styled-components](https://styled-components.com/docs)
to understand how individual components are built.


After adding an organisation story on contentful, one should run:
```shell
  yarn build:dev
```
for building with development environment variables, then
which will also automatically deploy on the develop instance.
You can also manually deploy to develop instance by:
```shell
  yarn deploy:dev
```

For production, 
```shell
  yarn build:prod
```
will build with production environment variables, but won't automatically deploy to the 
production instance.

For production deployment use
```shell
  yarn deploy:prod
```

Pages of new Stories will be automatically created by Gatsby after building with the above 
building commands. Also deploy the new build to ensure deployed instances are upto date.


### *Happy Hacking*.
